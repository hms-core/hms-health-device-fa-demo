/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare enum BleNotifyType {
    NOTIFICATION = "NOTIFICATION",
    INDICATION = "INDICATION"
}
interface BaseRequest {
    success?: (any?: any) => void;
    fail?: (code: number, error: Error) => void;
}
interface CharacteristicInfo {
    deviceId: string;
    serviceId: string;
}
interface BleCharacteristicData {
    characteristicId: string;
    characteristicProperty: BleCharacteristicProperty;
}
interface DeviceCharacteristicInfo {
    characteristicDataList: BleCharacteristicData[];
}
interface BleCharacteristicProperty {
    isReadable: boolean;
    isWriteable: boolean;
    isNotifySupported: boolean;
    isIndicateSupported: boolean;
}
interface BleServiceData {
    isPrimary: boolean;
    serviceUuid: string;
}
interface DeviceInfo {
    deviceId: string;
}
interface StartBleDeviceDiscoveryData {
    uuidServices?: string[];
    reportInterval?: number;
    timeoutInMillis?: number;
    isAllowDuplicatesKey?: boolean;
}
interface DiscoveredBleDevice {
    rawData: string;
    deviceId: string;
    serviceData: string;
    advertiseServiceUuids: string[];
}
interface BluetoothAdapterStateInfo {
    isAvailable: boolean;
    isDiscovering: boolean;
}
interface CreateBleConnectionInfo {
    deviceId: string;
    isAutoConnect: boolean;
}
interface BleConnectionStateInfo {
    deviceId: string;
    deviceName?: string;
    isConnected: boolean;
}
interface SetConnectionPriority {
    deviceId: string;
    connectionPriority: string;
}
interface ConnectionPriorityResult {
    deviceId: string;
    currentPriority: string;
}
interface DiscoveredBleDeviceService {
    deviceId: string;
    isDiscoverySuccess: boolean;
}
interface MtuUpdateData {
    deviceId: string;
    updatedMtu: number;
}
interface BleCharacteristicValueInfo {
    deviceId: string;
    serviceId: string;
    characteristicId: string;
    characteristicData?: string;
}
interface SetBleMTUInfo {
    deviceId: string;
    mtuValue: number;
}
interface ConnectedBleDeviceInfo {
    connectedDeviceIds: string[];
}
interface BleServiceDataInfo {
    deviceId: string;
    bleServiceDataList: BleServiceData[];
}
interface BleBaseOperation {
    deviceId: string;
    serviceId: string;
    characteristicId: string;
}
interface NotifyBleCharacteristicValueInfo extends BleBaseOperation {
    notifyType: BleNotifyType;
}
interface WifiStatusInfo {
    isConnected: boolean;
    isAvailable: boolean;
}
interface ConnectedWifiInfo {
    currentState: string;
    wifiBssid: string;
    wifiSsid: string;
}
interface WifiScannedInfo {
    wifiBssid: string;
    wifiSsid: string;
    wifiBand: string;
    bandWidth: string;
    wifiCapabilities: string;
}
interface ScannedWifiInfo {
    scanInformationList: WifiScannedInfo[];
}
interface CurrentWifiInfo {
    information: ConnectedWifiInfo;
}
interface HttpRequestData {
    httpUrl: string;
    requestData: string;
    requestType: string;
    requestHeaders: string;
}
interface HttpResponseData {
    responseData: string;
    errorMessage: string;
    message: string;
    httpCode: number;
    headers: string;
}
interface HttpRequestRequest extends BaseRequest, HttpRequestData {
    success: (info?: HttpResponseData) => void;
}
interface StartBleDiscoveryRequest extends BaseRequest, StartBleDeviceDiscoveryData {
}
interface BleDeviceCharacteristicRequest extends BaseRequest, CharacteristicInfo {
    success: (info?: DeviceCharacteristicInfo) => void;
}
interface PairDeviceRequest extends BaseRequest, DeviceInfo {
}
interface GetBluetoothAdapterStateRequest extends BaseRequest {
    success: (info: BluetoothAdapterStateInfo) => void;
}
interface CreateBleConnectionRequest extends BaseRequest, CreateBleConnectionInfo {
}
interface CloseBleConnectionRequest extends BaseRequest, DeviceInfo {
}
interface BleCharacteristicValueRequest extends BaseRequest, BleCharacteristicValueInfo {
}
interface NotifyBleCharacteristicValueRequest extends BaseRequest, NotifyBleCharacteristicValueInfo {
}
interface SetBleMTURequest extends BaseRequest, SetBleMTUInfo {
}
interface GetConnectedDevicesRequest extends BaseRequest {
    success: (info: ConnectedBleDeviceInfo) => void;
}
interface SetPriorityRequest extends BaseRequest, SetConnectionPriority {
    success: (info?: ConnectionPriorityResult) => void;
}
interface DeviceServiceRequest extends BaseRequest, DeviceInfo {
    success: (info: BleServiceDataInfo) => void;
}
interface ScannedWifiInfoRequest extends BaseRequest {
    success: (info: ScannedWifiInfo) => void;
}
interface ConnectedWifiInfoRequest extends BaseRequest {
    success: (info: CurrentWifiInfo) => void;
}
interface WifiStatusInfoRequest extends BaseRequest {
    success: (info: WifiStatusInfo) => void;
}
interface HttpRequestRequest extends BaseRequest, HttpRequestData {
}
declare class HealthEcologyOperator {
    private static getRequestAction;
    private static processOperation;
    static openBluetoothAdapter(request?: BaseRequest): Promise<void>;
    static closeBluetoothAdapter(request?: BaseRequest): Promise<void>;
    static startBleDevicesDiscovery(request?: StartBleDiscoveryRequest): Promise<void>;
    static stopBleDevicesDiscovery(request?: BaseRequest): Promise<void>;
    static onBleDevicesFound(callback: (dataObj: DiscoveredBleDevice[]) => void): Promise<void>;
    static onBluetoothAdapterStateChange(callback: (adapterState: BluetoothAdapterStateInfo) => void): Promise<void>;
    static getBluetoothAdapterState(request: GetBluetoothAdapterStateRequest): Promise<void>;
    static createBleConnection(request?: CreateBleConnectionRequest): Promise<void>;
    static closeBleConnection(request?: CloseBleConnectionRequest): Promise<void>;
    static onBleConnectionStateChange(callback: (dataObj: BleConnectionStateInfo) => void): Promise<void>;
    static onBleServiceDiscovered(callback: (dataObj: DiscoveredBleDeviceService) => void): Promise<void>;
    static onBleMtuUpdated(callback: (dataObj: MtuUpdateData) => void): Promise<void>;
    static readBleCharacteristicValue(request?: BleCharacteristicValueRequest): Promise<void>;
    static onBleCharacteristicValueRead(callback: (characteristicValueRead: BleCharacteristicValueInfo) => void): Promise<void>;
    static writeBleCharacteristicValue(request?: BleCharacteristicValueRequest): Promise<void>;
    static onBleCharacteristicValueWrite(callback: (characteristicValueWrite: BleCharacteristicValueInfo) => void): Promise<void>;
    static notifyBleCharacteristicValueChange(request?: NotifyBleCharacteristicValueRequest): Promise<void>;
    static onBleCharacteristicValueChange(callback: (characteristicValueChange: BleCharacteristicValueInfo) => void): Promise<void>;
    static setBleMTU(request?: SetBleMTURequest): Promise<void>;
    static setConnectionPriority(request: SetPriorityRequest): Promise<void>;
    static getBleDeviceServices(request: DeviceServiceRequest): Promise<void>;
    static getBleDeviceCharacteristics(request: BleDeviceCharacteristicRequest): Promise<void>;
    static getCurrentConnectedDevices(request?: GetConnectedDevicesRequest): Promise<void>;
    static getWifiStatus(request?: WifiStatusInfoRequest): Promise<void>;
    static startWifiScan(request?: BaseRequest): Promise<void>;
    static getConnectedWifiInformation(request?: ConnectedWifiInfoRequest): Promise<void>;
    static getScannedWifiInformation(request?: ScannedWifiInfoRequest): Promise<void>;
    static pairDevice(request: PairDeviceRequest): Promise<void>;
    static onHttpResponse(callback: (responseData: HttpResponseData) => void): Promise<void>;
    static processHttpRequest(request: HttpRequestRequest): Promise<void>;
    static syncProcessHttpRequest(request: HttpRequestRequest): Promise<void>;
}
export { HealthEcologyOperator, BleNotifyType };
