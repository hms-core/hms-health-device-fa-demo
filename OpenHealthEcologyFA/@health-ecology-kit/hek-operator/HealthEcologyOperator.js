/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BleNotifyType = exports.HealthEcologyOperator = void 0;
//For Java
const BUNDLE_NAME = 'com.huawei.healthecology';
const ABILITY_NAME = 'com.huawei.healthecology.client.JsFaClient';
const TAG = 'JS/Operator/BLE: ';
var RequestCode;
(function (RequestCode) {
    RequestCode[RequestCode["OPEN_BLUETOOTH_ADAPTER"] = 1001] = "OPEN_BLUETOOTH_ADAPTER";
    RequestCode[RequestCode["CLOSE_BLUETOOTH_ADAPTER"] = 1002] = "CLOSE_BLUETOOTH_ADAPTER";
    RequestCode[RequestCode["GET_BLUETOOTH_ADAPTER_STATE"] = 1003] = "GET_BLUETOOTH_ADAPTER_STATE";
    RequestCode[RequestCode["ON_BLUETOOTH_ADAPTER_STATE_CHANGE"] = 1004] = "ON_BLUETOOTH_ADAPTER_STATE_CHANGE";
    RequestCode[RequestCode["START_BLE_DEVICES_DISCOVERY"] = 1005] = "START_BLE_DEVICES_DISCOVERY";
    RequestCode[RequestCode["STOP_BLE_DEVICES_DISCOVERY"] = 1006] = "STOP_BLE_DEVICES_DISCOVERY";
    RequestCode[RequestCode["ON_BLE_DEVICE_FOUND"] = 1007] = "ON_BLE_DEVICE_FOUND";
    RequestCode[RequestCode["CREATE_BLE_CONNECTION"] = 1008] = "CREATE_BLE_CONNECTION";
    RequestCode[RequestCode["CLOSE_BLE_CONNECTION"] = 1009] = "CLOSE_BLE_CONNECTION";
    RequestCode[RequestCode["ON_BLE_CONNECTION_STATE_CHANGE"] = 1010] = "ON_BLE_CONNECTION_STATE_CHANGE";
    RequestCode[RequestCode["ON_BLE_SERVICE_DISCOVERED"] = 1011] = "ON_BLE_SERVICE_DISCOVERED";
    RequestCode[RequestCode["ON_BLE_MTU_UPDATED"] = 1012] = "ON_BLE_MTU_UPDATED";
    RequestCode[RequestCode["READ_BLE_CHARACTERISTIC_VALUE"] = 1013] = "READ_BLE_CHARACTERISTIC_VALUE";
    RequestCode[RequestCode["WRITE_BLE_CHARACTERISTIC_VALUE"] = 1014] = "WRITE_BLE_CHARACTERISTIC_VALUE";
    RequestCode[RequestCode["ON_BLE_CHARACTERISTIC_VALUE_CHANGE"] = 1015] = "ON_BLE_CHARACTERISTIC_VALUE_CHANGE";
    RequestCode[RequestCode["NOTIFY_BLE_CHARACTERISTIC_VALUE_CHANGE"] = 1016] = "NOTIFY_BLE_CHARACTERISTIC_VALUE_CHANGE";
    RequestCode[RequestCode["ON_BLE_CHARACTERISTIC_VALUE_READ"] = 1017] = "ON_BLE_CHARACTERISTIC_VALUE_READ";
    RequestCode[RequestCode["ON_BLE_CHARACTERISTIC_VALUE_WRITE"] = 1018] = "ON_BLE_CHARACTERISTIC_VALUE_WRITE";
    RequestCode[RequestCode["SET_BLE_CONNECTION_PRIORITY"] = 1020] = "SET_BLE_CONNECTION_PRIORITY";
    RequestCode[RequestCode["GET_CONNECTED_BLE_DEVICES"] = 1021] = "GET_CONNECTED_BLE_DEVICES";
    RequestCode[RequestCode["READ_BLE_DESCRIPTOR_VALUE"] = 1023] = "READ_BLE_DESCRIPTOR_VALUE";
    RequestCode[RequestCode["WRITE_BLE_DESCRIPTOR_VALUE"] = 1024] = "WRITE_BLE_DESCRIPTOR_VALUE";
    RequestCode[RequestCode["GET_BLE_CONNECTION_STATE"] = 1025] = "GET_BLE_CONNECTION_STATE";
    RequestCode[RequestCode["SET_BLE_MTU"] = 1026] = "SET_BLE_MTU";
    RequestCode[RequestCode["GET_DEVICE_SERVICES"] = 1027] = "GET_DEVICE_SERVICES";
    RequestCode[RequestCode["GET_DEVICE_CHARACTERISTICS"] = 1028] = "GET_DEVICE_CHARACTERISTICS";
    RequestCode[RequestCode["PAIR_BLUETOOTH_DEVICE"] = 1029] = "PAIR_BLUETOOTH_DEVICE";
    RequestCode[RequestCode["START_WIFI_SCAN"] = 1030] = "START_WIFI_SCAN";
    RequestCode[RequestCode["GET_WIFI_STATUS"] = 1031] = "GET_WIFI_STATUS";
    RequestCode[RequestCode["GET_CONNECTED_WIFI_INFORMATION"] = 1032] = "GET_CONNECTED_WIFI_INFORMATION";
    RequestCode[RequestCode["GET_SCANNED_WIFI_INFORMATION"] = 1033] = "GET_SCANNED_WIFI_INFORMATION";
    RequestCode[RequestCode["HTTP_NETWORK_REQUEST"] = 2001] = "HTTP_NETWORK_REQUEST";
    RequestCode[RequestCode["HTTP_NETWORK_ON_RESPONSE"] = 2002] = "HTTP_NETWORK_ON_RESPONSE";
    RequestCode[RequestCode["HTTP_SYNC_REQUEST"] = 2003] = "HTTP_SYNC_REQUEST";
})(RequestCode || (RequestCode = {}));
var ResponseCode;
(function (ResponseCode) {
    ResponseCode[ResponseCode["OPERATION_SUCCESS"] = 10000] = "OPERATION_SUCCESS";
    ResponseCode[ResponseCode["OPERATION_FAILED"] = 10001] = "OPERATION_FAILED";
    ResponseCode[ResponseCode["INTERNAL_ERROR"] = 10002] = "INTERNAL_ERROR";
    ResponseCode[ResponseCode["SYSTEM_ERROR"] = 10007] = "SYSTEM_ERROR";
    ResponseCode[ResponseCode["CREATE_CONNECTION_FAILED"] = 11002] = "CREATE_CONNECTION_FAILED";
    ResponseCode[ResponseCode["CLOSE_CONNECTION_FAILED"] = 11003] = "CLOSE_CONNECTION_FAILED";
    ResponseCode[ResponseCode["BLE_NOT_INITIALIZED"] = 11004] = "BLE_NOT_INITIALIZED";
    ResponseCode[ResponseCode["DEVICE_NOT_FOUND"] = 11006] = "DEVICE_NOT_FOUND";
    ResponseCode[ResponseCode["SERVICE_DISCOVERED"] = 11008] = "SERVICE_DISCOVERED";
    ResponseCode[ResponseCode["SERVICE_DISCOVER_FAILED"] = 11009] = "SERVICE_DISCOVER_FAILED";
    ResponseCode[ResponseCode["INVALID_SERVICE_UUID"] = 11010] = "INVALID_SERVICE_UUID";
    ResponseCode[ResponseCode["INVALID_SERVICE_OR_CHARACTERISTIC"] = 11011] = "INVALID_SERVICE_OR_CHARACTERISTIC";
    ResponseCode[ResponseCode["INVALID_CHARACTERISTIC_DESCRIPTOR"] = 11012] = "INVALID_CHARACTERISTIC_DESCRIPTOR";
    ResponseCode[ResponseCode["BLUETOOTH_PAIR_SUCCESS"] = 12002] = "BLUETOOTH_PAIR_SUCCESS";
    ResponseCode[ResponseCode["BLUETOOTH_PAIR_FAILED"] = 12003] = "BLUETOOTH_PAIR_FAILED";
    ResponseCode[ResponseCode["BLUETOOTH_NOT_INITIALIZED"] = 12004] = "BLUETOOTH_NOT_INITIALIZED";
    ResponseCode[ResponseCode["BLUETOOTH_NOT_AVAILABLE"] = 12005] = "BLUETOOTH_NOT_AVAILABLE";
    ResponseCode[ResponseCode["BLUETOOTH_DEVICE_NOT_FOUND"] = 12006] = "BLUETOOTH_DEVICE_NOT_FOUND";
    ResponseCode[ResponseCode["WIFI_NOT_INITIALIZED"] = 13004] = "WIFI_NOT_INITIALIZED";
})(ResponseCode || (ResponseCode = {}));
var BleNotifyType;
(function (BleNotifyType) {
    BleNotifyType["NOTIFICATION"] = "NOTIFICATION";
    BleNotifyType["INDICATION"] = "INDICATION";
})(BleNotifyType || (BleNotifyType = {}));
exports.BleNotifyType = BleNotifyType;
class HealthEcologyOperator {
    static getRequestAction(requestCode) {
        let requestAction = {
            bundleName: BUNDLE_NAME,
            abilityName: ABILITY_NAME,
            abilityType: 1 /* ABILITY_TYPE_INTERNAL */,
            syncOption: 0 /* ACTION_SYNC */,
            messageCode: requestCode,
        };
        return requestAction;
    }
    static processOperation(request, action) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield FeatureAbility.callAbility(action);
                console.debug(TAG + 'Operation[' + action.messageCode + ']: ' + result);
                let response = JSON.parse(result);
                if (response.code == ResponseCode.OPERATION_SUCCESS) {
                    request.success();
                }
                else {
                    let error = {
                        name: "Operation[" + action.messageCode + ']: Failed',
                        message: ResponseCode[response.code] === undefined ? "Unknown Error." : ResponseCode[response.code],
                    };
                    request.fail(response.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static openBluetoothAdapter(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.OPEN_BLUETOOTH_ADAPTER);
            this.processOperation(request, action);
        });
    }
    static closeBluetoothAdapter(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.CLOSE_BLUETOOTH_ADAPTER);
            this.processOperation(request, action);
        });
    }
    static startBleDevicesDiscovery(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.START_BLE_DEVICES_DISCOVERY);
            let actionData = {
                uuidServices: request.uuidServices != undefined ? request.uuidServices : [],
                reportInterval: request.reportInterval != undefined ? request.reportInterval : 0,
                timeoutInMillis: request.timeoutInMillis != undefined ? request.timeoutInMillis : 10000,
                isAllowDuplicatesKey: request.isAllowDuplicatesKey != undefined ? request.isAllowDuplicatesKey : false,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static stopBleDevicesDiscovery(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.STOP_BLE_DEVICES_DISCOVERY);
            this.processOperation(request, action);
        });
    }
    static onBleDevicesFound(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_DEVICE_FOUND);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Bluetooth device discovered: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + resultStr);
        });
    }
    static onBluetoothAdapterStateChange(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLUETOOTH_ADAPTER_STATE_CHANGE);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Bluetooth adapter state changed: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + resultStr);
        });
    }
    static getBluetoothAdapterState(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_BLUETOOTH_ADAPTER_STATE);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.error("Bluetooth adapter state result is :" + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "Get bluetoothAdapterState Error",
                        message: ResponseCode[resultObj.code] === undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static createBleConnection(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.CREATE_BLE_CONNECTION);
            let actionData = {
                deviceId: request.deviceId,
                isAutoConnect: request.isAutoConnect != undefined ? request.isAutoConnect : false,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static closeBleConnection(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.CLOSE_BLE_CONNECTION);
            let actionData = {
                deviceId: request.deviceId,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static onBleConnectionStateChange(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_CONNECTION_STATE_CHANGE);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
        });
    }
    static onBleServiceDiscovered(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_SERVICE_DISCOVERED);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                let resultObj = JSON.parse(callbackData);
                callback(resultObj.data);
            });
            console.debug(TAG + 'Ble service discover change result is:' + resultStr);
        });
    }
    static onBleMtuUpdated(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_MTU_UPDATED);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
        });
    }
    static readBleCharacteristicValue(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.READ_BLE_CHARACTERISTIC_VALUE);
            let actionData = {
                deviceId: request.deviceId,
                serviceId: request.serviceId,
                characteristicId: request.characteristicId
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static onBleCharacteristicValueRead(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_CHARACTERISTIC_VALUE_READ);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Ble characteristic value read: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + 'Read Ble characteristic value result is:' + resultStr);
        });
    }
    static writeBleCharacteristicValue(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.WRITE_BLE_CHARACTERISTIC_VALUE);
            let actionData = {
                deviceId: request.deviceId,
                serviceId: request.serviceId,
                characteristicId: request.characteristicId,
                characteristicData: request.characteristicData,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static onBleCharacteristicValueWrite(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_CHARACTERISTIC_VALUE_READ);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Ble characteristic value write: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + 'Write Ble characteristic value result is:' + resultStr);
        });
    }
    static notifyBleCharacteristicValueChange(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.NOTIFY_BLE_CHARACTERISTIC_VALUE_CHANGE);
            let actionData = {
                deviceId: request.deviceId,
                serviceId: request.serviceId,
                characteristicId: request.characteristicId,
                notifyType: request.notifyType,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static onBleCharacteristicValueChange(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_BLE_CHARACTERISTIC_VALUE_CHANGE);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Ble characteristic value change: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + resultStr);
        });
    }
    static setBleMTU(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.SET_BLE_MTU);
            let actionData = {
                deviceId: request.deviceId,
                mtuValue: request.mtuValue,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static setConnectionPriority(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.SET_BLE_CONNECTION_PRIORITY);
                let actionData = {
                    deviceId: request.deviceId,
                    connectionPriority: request.connectionPriority,
                };
                action.data = actionData;
                let resultString = yield FeatureAbility.callAbility(action);
                let resultResponse = JSON.parse(resultString);
                console.debug(TAG + "Connection priority result is : " + resultResponse.code);
                if (resultResponse.code == ResponseCode.OPERATION_SUCCESS) {
                    let response = JSON.parse(resultResponse.data);
                    request.success(response);
                }
                else {
                    let error = {
                        name: "SET CONNECTION PRIORITY ERROR",
                        message: RequestCode[resultResponse.code] == undefined ? "Unknown Error." : RequestCode[resultResponse.code]
                    };
                    request.fail(resultResponse.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getBleDeviceServices(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_DEVICE_SERVICES);
                action.data = request.deviceId;
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + "Ble device services result is : " + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET DEVICE SERVICES ERROR",
                        message: RequestCode[resultObj.code] == undefined ? "Unknown Error." : RequestCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getBleDeviceCharacteristics(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_DEVICE_CHARACTERISTICS);
                let actionData = {
                    deviceId: request.deviceId,
                    serviceId: request.serviceId,
                };
                action.data = actionData;
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + "Ble device characteristics result is : " + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET DEVICE CHARACTERISTICS ERROR",
                        message: RequestCode[resultObj.code] == undefined ? "Unknown Error." : RequestCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getCurrentConnectedDevices(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_CONNECTED_BLE_DEVICES);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Current connected devices result is: ' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET CONNECTED DEVICES Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getWifiStatus(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_WIFI_STATUS);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Wifi status result is: ' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET WIFI STATUS Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static startWifiScan(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.START_WIFI_SCAN);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Wifi scan result is:' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    request.success();
                }
                else {
                    let error = {
                        name: "START WIFI SCAN Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getConnectedWifiInformation(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_CONNECTED_WIFI_INFORMATION);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Connected Wifi information result is: ' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET CONNECTED WIFI INFORMATION Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static getScannedWifiInformation(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.GET_SCANNED_WIFI_INFORMATION);
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Scanned Wifi information result is:' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "GET SCANNED WIFI INFORMATION Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
    static pairDevice(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.PAIR_BLUETOOTH_DEVICE);
            action.data = request.deviceId;
            this.processOperation(request, action);
        });
    }
    static onHttpResponse(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.HTTP_NETWORK_ON_RESPONSE);
            let resultStr = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                console.debug(TAG + 'Http response: ' + callbackData);
                let callbackJson = JSON.parse(callbackData);
                callback(callbackJson.data);
            });
            console.debug(TAG + resultStr);
        });
    }
    static processHttpRequest(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.HTTP_NETWORK_REQUEST);
            let actionData = {
                httpUrl: request.httpUrl,
                requestData: request.requestData,
                requestHeaders: request.requestHeaders,
                requestType: request.requestType
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static syncProcessHttpRequest(request) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let action = this.getRequestAction(RequestCode.HTTP_SYNC_REQUEST);
                let actionData = {
                    httpUrl: request.httpUrl,
                    requestData: request.requestData,
                    requestHeaders: request.requestHeaders,
                    requestType: request.requestType
                };
                action.data = actionData;
                let resultStr = yield FeatureAbility.callAbility(action);
                let resultObj = JSON.parse(resultStr);
                console.debug(TAG + 'Http request result is:' + resultObj.code);
                if (resultObj.code == ResponseCode.OPERATION_SUCCESS) {
                    let stateObj = JSON.parse(resultObj.data);
                    request.success(stateObj);
                }
                else {
                    let error = {
                        name: "Http Error",
                        message: ResponseCode[resultObj.code] == undefined ? "Unknown Error." : ResponseCode[resultObj.code]
                    };
                    request.fail(resultObj.code, error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
            }
        });
    }
}
exports.HealthEcologyOperator = HealthEcologyOperator;
