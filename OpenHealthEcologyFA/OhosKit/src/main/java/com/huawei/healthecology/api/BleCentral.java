/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.api;

import com.huawei.healthecology.data.ble.callback.AdapterStateChangeCallback;
import com.huawei.healthecology.data.ble.callback.BleDeviceFoundCallback;
import com.huawei.healthecology.data.ble.request.BleDeviceDiscoverRequest;
import com.huawei.healthecology.data.ble.response.BleResponseCode;
import com.huawei.healthecology.data.ble.response.ConnectedDeviceResponse;
import com.huawei.healthecology.data.ble.response.ConnectionStateResponse;
import com.huawei.healthecology.data.bluetooth.response.BluetoothAdapterStateResponse;
import com.huawei.healthecology.data.bluetooth.response.BluetoothResponseCode;

/**
 * Ble Central interface
 */
public interface BleCentral {
    /**
     * Registering bluetooth adapter state callback
     *
     * @param stateChangeCallback Callback
     * @return errorCode
     */
    BluetoothResponseCode onBluetoothAdapterStateChange(AdapterStateChangeCallback stateChangeCallback);

    /**
     * Turn off bluetooth adapter state change
     *
     * @return errorCode
     */
    BluetoothResponseCode offBluetoothAdapterStateChange();

    /**
     * Turn on the device Bluetooth adapter
     *
     * @return true if success, false otherwise
     */
    BluetoothResponseCode openBluetoothAdapter();

    /**
     * Turn off the device Bluetooth adapter
     *
     * @return true if success, false otherwise
     */
    BluetoothResponseCode closeBluetoothAdapter();

    /**
     * Get bluetooth state
     *
     * @return errorCode
     */
    BluetoothAdapterStateResponse getBluetoothAdapterState();

    /**
     * Registering Ble device found callback
     *
     * @param callback Device found callback
     * @return errorCode
     */
    BleResponseCode onBleDevicesFound(BleDeviceFoundCallback callback);

    /**
     * Start Bluetooth Scan
     *
     * @param request Discover Request
     * @return errorCode
     */
    BleResponseCode startBleDevicesDiscovery(BleDeviceDiscoverRequest request);

    /**
     * Stop searching for nearby Bluetooth devices
     *
     * @return errorCode
     */
    BleResponseCode stopBleDevicesDiscovery();

    /**
     * To get current connected devices
     *
     * @return The response contains id of connected devices
     */
    ConnectedDeviceResponse getCurrentConnectedDevices();

    /**
     * Check whether the BLE state of device is connected or not.
     *
     * @param deviceId Mac address of device
     * @return connection state
     */
    ConnectionStateResponse getBleConnectionState(String deviceId);
}
