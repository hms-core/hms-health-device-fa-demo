/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.api;

import com.huawei.healthecology.data.ble.callback.BleCharacteristicValueChangeCallback;
import com.huawei.healthecology.data.ble.callback.BleConnectionChangeCallback;
import com.huawei.healthecology.data.ble.callback.BleMtuUpdatedCallback;
import com.huawei.healthecology.data.ble.callback.BleServiceDiscoveredCallback;
import com.huawei.healthecology.data.ble.callback.ReadBleCharacteristicValueCallback;
import com.huawei.healthecology.data.ble.callback.WriteBleCharacteristicValueCallback;
import com.huawei.healthecology.data.ble.request.BleConnectionPriorityRequest;
import com.huawei.healthecology.data.ble.request.BleDeviceCharacteristicGetRequest;
import com.huawei.healthecology.data.ble.request.BleDeviceConnectRequest;
import com.huawei.healthecology.data.ble.request.BleDeviceConnectionCloseRequest;
import com.huawei.healthecology.data.ble.request.BleMtuSetRequest;
import com.huawei.healthecology.data.ble.request.CharacteristicNotifyRequest;
import com.huawei.healthecology.data.ble.request.CharacteristicReadRequest;
import com.huawei.healthecology.data.ble.request.CharacteristicWriteRequest;
import com.huawei.healthecology.data.ble.request.DescriptorReadRequest;
import com.huawei.healthecology.data.ble.request.DescriptorWriteRequest;
import com.huawei.healthecology.data.ble.response.BleResponseCode;
import com.huawei.healthecology.data.ble.response.ConnectionPriorityResponse;
import com.huawei.healthecology.data.ble.response.DeviceCharacteristicGetResponse;
import com.huawei.healthecology.data.ble.response.DeviceServiceGetResponse;

/**
 * Ble Generic Attribute Profile
 */
public interface BleGatt {
    /**
     * Create Ble bluetooth connection
     *
     * @param connectRequest Connect request
     * @return errorCode
     */
    BleResponseCode createBleConnection(BleDeviceConnectRequest connectRequest);

    /**
     * Close Ble bluetooth connection
     *
     * @param closeRequest Connection Close request
     * @return error code
     */
    BleResponseCode closeBleConnection(BleDeviceConnectionCloseRequest closeRequest);

    /**
     * Registering connection state change callback
     *
     * @param connectionStateChangeCallback Callback
     */
    void onBleConnectionChange(BleConnectionChangeCallback connectionStateChangeCallback);

    /**
     * Registering BleService discovered callback
     *
     * @param discoveredCallback Callback
     */
    void onBleServiceDiscovered(BleServiceDiscoveredCallback discoveredCallback);

    /**
     * Registering mtu uprate callback
     *
     * @param mtuUpdatedCallback Callback
     */
    void onBleMtuUpdated(BleMtuUpdatedCallback mtuUpdatedCallback);

    /**
     * Registering Ble characteristic value read callback
     *
     * @param readCallback Callback
     */
    void onBleCharacteristicValueRead(ReadBleCharacteristicValueCallback readCallback);

    /**
     * Registering Ble characteristic value write callback
     *
     * @param writeCallback Callback
     */
    void onBleCharacteristicValueWrite(WriteBleCharacteristicValueCallback writeCallback);

    /**
     * Registering Ble characteristic value change callback
     *
     * @param characteristicValueChangeCallback Callback
     */
    void onBleCharacteristicValueChange(BleCharacteristicValueChangeCallback characteristicValueChangeCallback);

    /**
     * Read Ble characteristic value
     *
     * @param readRequest Request
     * @return errorCode
     */
    BleResponseCode readBleCharacteristicValue(CharacteristicReadRequest readRequest);

    /**
     * Write Ble characteristic value
     *
     * @param writeRequest Request
     * @return errorCode
     */
    BleResponseCode writeBleCharacteristicValue(CharacteristicWriteRequest writeRequest);

    /**
     * Notify Ble characteristic value change
     *
     * @param notifyRequest Request
     * @return errorCode
     */
    BleResponseCode notifyBleCharacteristicValueChange(CharacteristicNotifyRequest notifyRequest);

    /**
     * Read Ble descriptor value
     *
     * @param readRequest Request
     * @return errorCode
     */
    BleResponseCode readBleDescriptorValue(DescriptorReadRequest readRequest);

    /**
     * Write Ble descriptor value
     *
     * @param writeRequest Request
     * @return errorCode
     */
    BleResponseCode writeBleDescriptorValue(DescriptorWriteRequest writeRequest);

    /**
     * To set the connection priority
     *
     * @param connectionPriorityRequest The priority request
     * @return The response code presents request result
     */
    ConnectionPriorityResponse setConnectionPriority(BleConnectionPriorityRequest connectionPriorityRequest);

    /**
     * Get services of Ble device
     *
     * @param deviceId Device Id
     * @return response
     */
    DeviceServiceGetResponse getBleDeviceServices(String deviceId);

    /**
     * Get characteristics of Ble device
     *
     * @param request Request
     * @return response
     */
    DeviceCharacteristicGetResponse getBleDeviceCharacteristics(BleDeviceCharacteristicGetRequest request);

    /**
     * Set Ble device Mtu
     *
     * @param request Request
     * @return errorCode
     */
    BleResponseCode setBleMtu(BleMtuSetRequest request);
}
