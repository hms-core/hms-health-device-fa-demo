/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.api;

import com.huawei.healthecology.data.wifi.response.WifiConnectionResponse;
import com.huawei.healthecology.data.wifi.response.WifiResponseCode;
import com.huawei.healthecology.data.wifi.response.WifiScanningResponse;
import com.huawei.healthecology.data.wifi.response.WifiStatusResponse;

/**
 * Wifi central interface
 */
public interface WifiCentral {
    /**
     * To start wifi scanning
     *
     * @return Code to indicates the operation result
     */
    WifiResponseCode startScanning();

    /**
     * To get the current wifi status
     *
     * @return Response to present the current status
     */
    WifiStatusResponse getWifiStatus();

    /**
     * To get the current connected wifi information
     *
     * @return Response to present the current connected information
     */
    WifiConnectionResponse getConnectedInformation();

    /**
     * To get last scannced wifi information
     *
     * @return Response to present the scanned information
     */
    WifiScanningResponse getScannedInformation();
}
