/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.client;

import com.huawei.healthecology.data.platform.PlatformRequest;
import com.huawei.healthecology.json.JsonMapperType;
import com.huawei.healthecology.log.LogUtil;
import com.huawei.healthecology.module.ClientModule;
import com.huawei.healthecology.operator.JsRemoteOperator;
import com.huawei.healthecology.operator.PlatformOperationCode;

import lombok.Builder;
import ohos.ace.ability.AceInternalAbility;
import ohos.app.AbilityContext;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;

import java.util.Optional;

/**
 * The Kit client for Js FA
 */
public class JsFaClient extends AceInternalAbility {
    private static final String TAG = "HealthEcologyOhosClient";

    private static final String BUNDLE_NAME = "com.huawei.healthecology";

    private static final String ABILITY_NAME = "com.huawei.healthecology.client.JsFaClient";

    /**
     * The builder constructor for JsFaClient
     *
     * @param abilityContext The ability context
     * @param mapperType The mapper type
     */
    @Builder
    public JsFaClient(AbilityContext abilityContext, JsonMapperType mapperType) {
        super(BUNDLE_NAME, ABILITY_NAME);
        setInternalAbilityHandler(this::onProcessRequest);
        Optional.ofNullable(mapperType).ifPresent(ClientModule::configJsonMapper);
    }

    private boolean onProcessRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
        LogUtil.info(TAG, "onProcessRequest code:" + code);
        String requestValue = data.readString();
        return PlatformOperationCode.processJsFaOperation(
            JsRemoteOperator.of(data.readRemoteObject(), reply),
            PlatformRequest.builder()
                .requestCode(code)
                .requestValue(requestValue)
                .build());
    }
}
