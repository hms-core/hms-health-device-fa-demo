/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.client;

import com.huawei.healthecology.data.platform.PlatformOperation;
import com.huawei.healthecology.data.platform.PlatformResponse;
import com.huawei.healthecology.json.JsonMapperType;
import com.huawei.healthecology.log.LogUtil;
import com.huawei.healthecology.module.ClientModule;
import com.huawei.healthecology.operator.PlatformOperationCode;

import lombok.Builder;
import lombok.NonNull;

import java.util.Optional;

/**
 * The kit client for Java operation
 */
public class OhosNativeClient {
    private static final String TAG = "NativeClient";

    /**
     * The builder constructor for OhosNativeClient.
     *
     * @param mapperType The mapper type
     */
    @Builder
    public OhosNativeClient(JsonMapperType mapperType) {
        Optional.ofNullable(mapperType).ifPresent(ClientModule::configJsonMapper);
    }

    /**
     * To process the Js FA request
     *
     * @param operation The platform operation
     * @return The platform response
     */
    public PlatformResponse onProcessRequest(@NonNull PlatformOperation operation) {
        LogUtil.info(TAG, "onProcessRequest code:" + operation.getRequestCode());
        return PlatformOperationCode.processNativeOperation(operation);
    }
}
