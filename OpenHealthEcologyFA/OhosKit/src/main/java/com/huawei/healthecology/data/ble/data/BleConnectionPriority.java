/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.ble.data;

import com.google.gson.annotations.SerializedName;

/**
 * Ble device connection priority
 */
public enum BleConnectionPriority {
    /**
     * Indicates the normal ble connection priority
     */
    @SerializedName("normal")
    NORMAL_PRIORITY ("normal"),

    /**
     * Indicates the high ble connection priority
     */
    @SerializedName("high")
    HIGH_PRIORITY ("high"),

    /**
     * Indicates the low ble connection priority
     */
    @SerializedName("low")
    LOW_PRIORITY ("low"),
    ;

    private final String value;

    BleConnectionPriority(String value) {
        this.value = value;
    }

    /**
     * Parse the connection priority to the OS priority constants
     *
     * @param priority Priority constant
     * @return The valid priority constants, default to <tt>BleConnectionPriority.NORMAL_PRIORITY</tt>.
     */
    public static BleConnectionPriority parseTo(String priority) {
        for (BleConnectionPriority connectionPriority : BleConnectionPriority.values()) {
            if (connectionPriority.value.equalsIgnoreCase(priority)) {
                return connectionPriority;
            }
        }
        return NORMAL_PRIORITY;
    }
}
