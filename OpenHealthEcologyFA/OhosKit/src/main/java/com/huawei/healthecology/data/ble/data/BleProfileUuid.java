/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.ble.data;

import java.util.UUID;

/**
 * Ble profile uuid
 */
public enum BleProfileUuid {
    BLOOD_PRESSURE_SERVICE("00001810-0000-1000-8000-00805f9b34fb"),

    BLOOD_PRESSURE_CHARACTERISTIC("00002a35-0000-1000-8000-00805f9b34fb"),

    CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR("00002902-0000-1000-8000-00805f9b34fb"),

    BLOOD_INTERMEDIATE_CUFF_PRESSURE_CHARACTERISTIC("00002a36-0000-1000-8000-00805f9b34fb"),

    GLUCOSE_SERVICE("00001808-0000-1000-8000-00805f9b34fb"),

    GLUCOSE_MEASUREMENT_CHARACTERISTIC("00002A18-0000-1000-8000-00805f9b34fb"),

    GLUCOSE_RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC("00002A52-0000-1000-8000-00805f9b34fb"),

    CHARACTERISTIC_CURRENT_TIME("00002a2b-0000-1000-8000-00805f9b34fb"),

    UNKNOWN_UUID("UNKNOWN_UUID");

    private final String uuidValue;

    BleProfileUuid(String value) {
        this.uuidValue = value;
    }

    /**
     * Generate UUID instance by uuidValue
     *
     * @return UUID
     */
    public UUID getUuid() {
        return UUID.fromString(this.uuidValue);
    }

    /**
     * Get uuid value
     *
     * @return Uuid value
     */
    public String getValue() {
        return this.uuidValue;
    }
}
