/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.ble.data;

import java.util.Arrays;

/**
 * Bluetooth event action
 */
public enum BluetoothEventAction {
    /**
     * Host name update
     */
    HOST_NAME_UPDATE("usual.event.bluetooth.host.NAME_UPDATE"),

    /**
     * Discovery started
     */
    DISCOVERY_STARTED("usual.event.bluetooth.host.DISCOVERY_STARTED"),

    /**
     * Discovery finished
     */
    DISCOVERY_FINISHED("usual.event.bluetooth.host.DISCOVERY_FINISHED"),

    /**
     * Unknown event
     */
    UNKNOWN_EVENT("Unknown");

    private final String action;

    BluetoothEventAction(String value) {
        this.action = value;
    }

    /**
     * Convert string event to BluetoothEventAction
     *
     * @param event Event string
     * @return BluetoothEventAction
     */
    public static BluetoothEventAction parseFrom(String event) {
        return Arrays.stream(BluetoothEventAction.values())
            .filter(eventAction -> eventAction.action.equalsIgnoreCase(event))
            .findAny()
            .orElse(UNKNOWN_EVENT);
    }
}
