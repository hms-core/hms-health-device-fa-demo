/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.ble.response;

import com.huawei.healthecology.data.ResponseCode;

/**
 * Ble response code
 */
public enum BleResponseCode implements ResponseCode {
    OPERATION_SUCCESS(1_0000),

    OPERATION_FAILED(1_0001),

    INTERNAL_ERROR(1_0002),

    INVALID_REQUEST_FORMAT(1_0003),

    SYSTEM_ERROR(1_0007),

    CREATE_CONNECTION_FAILED(1_1002),

    CLOSE_CONNECTION_FAILED(1_1003),

    NOT_INITIALIZED(1_1004),

    DEVICE_NOT_FOUND(1_1006),

    SERVICE_DISCOVERED(1_1008),

    SERVICE_DISCOVER_FAILED(1_1009),

    INVALID_SERVICE_UUID(1_1010),

    INVALID_SERVICE_OR_CHARACTERISTIC(1_1011),

    INVALID_CHARACTERISTIC_DESCRIPTOR(1_1012),

    UNKNOWN_ERROR(0);

    private final int value;

    BleResponseCode(int code) {
        this.value = code;
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
