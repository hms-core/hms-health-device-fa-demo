/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.ble.response;

import com.huawei.healthecology.data.HealthEcologyResponse;
import com.huawei.healthecology.data.ble.data.BleServiceData;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

/**
 * Get device service response
 */
@Getter
public class DeviceServiceGetResponse extends HealthEcologyResponse<BleResponseCode> {
    private final String deviceId;

    private final List<BleServiceData> bleServiceDataList;

    /**
     * Get device service response
     *
     * @param responseCode The ble device response code
     * @param deviceId The operated device id
     * @param bleServiceDataList The ble device data list
     */
    @Builder
    public DeviceServiceGetResponse(BleResponseCode responseCode,
        String deviceId, List<BleServiceData> bleServiceDataList) {
        super(responseCode);
        this.deviceId = deviceId;
        this.bleServiceDataList = bleServiceDataList;
    }
}
