/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.bluetooth.response;

import com.huawei.healthecology.data.HealthEcologyResponse;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Bluetooth adapter state response
 */
@Setter
@Getter
@ToString
public class BluetoothAdapterStateResponse extends HealthEcologyResponse<BluetoothResponseCode> {
    private final boolean isDiscovering;

    private final boolean isAvailable;

    /**
     * Bluetooth adapter state response
     *
     * @param responseCode bluetooth response code
     * @param isDiscovering is device discovery
     * @param isAvailable is device available
     */
    @Builder
    public BluetoothAdapterStateResponse(BluetoothResponseCode responseCode,
        boolean isDiscovering, boolean isAvailable) {
        super(responseCode);
        this.isAvailable = isAvailable;
        this.isDiscovering = isDiscovering;
    }
}
