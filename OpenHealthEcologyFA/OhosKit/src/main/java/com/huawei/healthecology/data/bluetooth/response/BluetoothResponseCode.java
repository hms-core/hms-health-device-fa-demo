/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.bluetooth.response;

import com.huawei.healthecology.data.ResponseCode;

/**
 * Bluetooth response code
 */
public enum BluetoothResponseCode implements ResponseCode {
    OPERATION_SUCCESS(1_0000),

    OPERATION_FAILED(1_0001),

    INTERNAL_ERROR(1_0002),

    SYSTEM_ERROR(1_0007),

    PAIR_DEVICE_FAILED(1_2003),

    NOT_INITIALIZED(1_2004),

    BLUETOOTH_NOT_AVAILABLE(1_2005),

    BLUETOOTH_DEVICE_NOT_FOUND(1_2006),

    UNKNOWN_ERROR(0);

    private final int value;

    BluetoothResponseCode(int code) {
        this.value = code;
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
