/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.http.response;

import com.huawei.healthecology.data.HealthEcologyResponse;
import com.huawei.healthecology.data.http.HttpResponseCode;
import com.huawei.healthecology.data.http.HttpResponseData;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Http request response
 */
@Setter
@Getter
@ToString
public class HttpRequestResponseData extends HealthEcologyResponse<HttpResponseCode> {
    private final HttpResponseData httpResponseData;

    /**
     * Http request response
     *
     * @param responseCode     http operate response code
     * @param httpResponseData http response data
     */
    @Builder
    public HttpRequestResponseData(HttpResponseCode responseCode, HttpResponseData httpResponseData) {
        super(responseCode);
        this.httpResponseData = httpResponseData;
    }
}
