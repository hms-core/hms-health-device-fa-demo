/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.platform;

import lombok.experimental.SuperBuilder;

import java.util.Optional;

/**
 * The
 */
@SuperBuilder
public class H5PlatformRequest extends PlatformRequest {
    private final String remoteMethod;

    public Optional<String> getRemoteMethod() {
        return Optional.ofNullable(remoteMethod);
    }
}
