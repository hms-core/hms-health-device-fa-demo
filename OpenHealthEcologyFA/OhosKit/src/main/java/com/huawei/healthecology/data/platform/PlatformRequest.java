/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.platform;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

/**
 * The request used for requesting the operation crossing the platform
 */
@SuperBuilder
public class PlatformRequest {
    @Getter
    private final int requestCode;

    private final String requestValue;

    /**
     * To get the raw string request in Json format
     *
     * @return A valid Json string if available
     */
    public Optional<String> getRawRequest() {
        return Optional.ofNullable(requestValue);
    }
}
