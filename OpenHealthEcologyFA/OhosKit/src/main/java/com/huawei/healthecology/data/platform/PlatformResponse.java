/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.platform;

import com.huawei.healthecology.data.HealthEcologyResponse;
import com.huawei.healthecology.data.ResponseCode;
import com.huawei.healthecology.module.ClientModule;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * PlatformResponse
 */
@Getter
@Builder
@ToString
public class PlatformResponse {
    @SerializedName("code")
    private final int resultCode;

    @SerializedName("data")
    private final String resultData;

    /**
     * The override platform response builder
     */
    public static class PlatformResponseBuilder {
        /**
         * The builder for result code and result data
         *
         * @param response The response in health ecology response format
         * @param <T> The response code type
         * @return The response builder
         */
        public <T extends ResponseCode> PlatformResponseBuilder responseValue(
            @NonNull HealthEcologyResponse<T> response) {
            this.resultCode = response.getResponseCode().getValue();
            this.resultData = ClientModule.getParser()
                .flatMap(parser -> parser.stringify(response))
                .orElse(null);
            return this;
        }

        /**
         * The builder for response code
         *
         * @param responseCode The result response code
         * @param <T> The response code type
         * @return The response builder
         */
        public <T extends ResponseCode> PlatformResponseBuilder responseCode(@NonNull T responseCode) {
            this.resultCode = responseCode.getValue();
            return this;
        }
    }
}
