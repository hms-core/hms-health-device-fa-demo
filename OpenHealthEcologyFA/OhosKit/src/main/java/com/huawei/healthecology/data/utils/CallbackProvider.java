/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.utils;

import lombok.Builder;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Callback provider
 *
 * @param <T> Callback generic type callback
 */
public class CallbackProvider<T> {
    private final Set<T> callbackSet;

    /**
     * Constructor
     *
     * @param callback Generic type callback
     */
    @Builder
    public CallbackProvider(T callback) {
        callbackSet = new LinkedHashSet<>();
        Optional.ofNullable(callback).ifPresent(callbackSet::add);
    }

    /**
     * Constructor
     *
     * @param callback Generic type callback
     * @return CallbackProvider
     */
    public CallbackProvider<T> add(T callback) {
        Optional.ofNullable(callback).ifPresent(callbackSet::add);
        return this;
    }

    /**
     * Get last callback
     *
     * @return Last callback
     */
    public Optional<T> getLast() {
        return callbackSet.stream().reduce((first, second) -> second);
    }

    /**
     * Get all callback
     *
     * @return All callback
     */
    public Stream<T> getAll() {
        return callbackSet.stream();
    }

    /**
     * To clear all registered callback
     */
    public void clear() {
        callbackSet.clear();
    }
}
