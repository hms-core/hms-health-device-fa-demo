/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.utils;

import java.util.Arrays;

/**
 * StringUtils
 */
public class StringUtils {
    private StringUtils() {
    }

    /**
     * Check CharSequence is empty
     *
     * @param sequence CharSequence
     * @return if not empty return true else false
     */
    public static boolean isEmpty(CharSequence sequence) {
        if (sequence == null) {
            return true;
        } else {
            return sequence.length() == 0;
        }
    }

    /**
     * Check CharSequences is empty
     *
     * @param sequences variable parameter
     * @return if not empty return true else false
     */
    public static boolean isEmptySequences(CharSequence... sequences) {
        return Arrays.stream(sequences).allMatch(StringUtils::isEmpty);
    }
}
