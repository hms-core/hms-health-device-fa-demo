/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.wifi.data;

/**
 * Wifi bandwidth
 */
public enum WifiBandWidth {
    /**
     * 20MHz
     */
    BAND_WIDTH_20MHZ(0),

    /**
     * 40MHz
     */
    BAND_WIDTH_40MHZ(1),

    /**
     * 80MHz
     */
    BAND_WIDTH_80MHZ(2),

    /**
     * 160MHz
     */
    BAND_WIDTH_160MHZ(3),

    /**
     * 80MHz plus
     */
    BAND_WIDTH_80MHZ_PLUS(4),

    /**
     * Unknown bandwidth
     */
    UNKNOWN_BAND_WIDTH(-1);

    private int value;

    WifiBandWidth(int width) {
        this.value = width;
    }

    /**
     * Convert index to WifiBandWidth
     *
     * @param value index
     * @return WifiBandWidth
     */
    public static WifiBandWidth parseTo(int value) {
        for (WifiBandWidth bandWidth : WifiBandWidth.values()) {
            if (bandWidth.value == value) {
                return bandWidth;
            }
        }
        return UNKNOWN_BAND_WIDTH;
    }
}
