/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.wifi.response;

import com.huawei.healthecology.data.HealthEcologyResponse;
import com.huawei.healthecology.data.wifi.data.WifiConnectedInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Wifi connection response
 */
@Getter
@ToString
public class WifiConnectionResponse extends HealthEcologyResponse<WifiResponseCode> {
    private final WifiConnectedInfo information;

    /**
     * Constructor
     *
     * @param responseCode wifi response code
     * @param information wifi connected info
     */
    @Builder
    public WifiConnectionResponse(WifiResponseCode responseCode, WifiConnectedInfo information) {
        super(responseCode);
        this.information = information;
    }
}
