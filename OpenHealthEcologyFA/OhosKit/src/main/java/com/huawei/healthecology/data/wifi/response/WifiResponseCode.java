/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.wifi.response;

import com.huawei.healthecology.data.ResponseCode;

/**
 * Wifi response code
 */
public enum WifiResponseCode implements ResponseCode {
    /**
     * Operation success
     */
    OPERATION_SUCCESS(1_0000),

    /**
     * Operation failed
     */
    OPERATION_FAILED(1_0001),

    /**
     * Internal error
     */
    INTERNAL_ERROR(1_0002),

    /**
     * System error
     */
    SYSTEM_ERROR(1_0007),

    /**
     * Not initialized
     */
    NOT_INITIALIZED(1_3004),

    /**
     * Unknown error
     */
    UNKNOWN_ERROR(0);

    private final int value;

    WifiResponseCode(int code) {
        this.value = code;
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
