/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.data.wifi.response;

import com.huawei.healthecology.data.HealthEcologyResponse;

import lombok.Builder;
import lombok.Getter;

/**
 * Wifi status response
 */
@Getter
public class WifiStatusResponse extends HealthEcologyResponse<WifiResponseCode> {
    private final boolean isConnected;

    private final boolean isAvailable;

    /**
     * Constructor
     *
     * @param responseCode wifi response code
     * @param isConnected is wifi connected
     * @param isAvailable is wifi available
     */
    @Builder
    public WifiStatusResponse(WifiResponseCode responseCode, boolean isConnected, boolean isAvailable) {
        super(responseCode);
        this.isConnected = isConnected;
        this.isAvailable = isAvailable;
    }
}
