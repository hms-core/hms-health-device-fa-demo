/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.json;

import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Optional;

/**
 * Json mapper
 */
public interface JsonMapper {
    /**
     * Map a JSON string to a Java type.
     *
     * @param str The JSON string to map.
     * @param type The target type.
     * @return The mapped object.
     */
    <T> Optional<T> mapTo(String str, Class<T> type);

    /**
     * To get the json map
     *
     * @param string The json String
     * @return A valid parsed map
     */
    Optional<Map<String, String>> getJsonMap(String string);

    /**
     * Map a JSON stream to a Java type.
     *
     * @param stream The JSON stream to map.
     * @param type The target type.
     * @return The mapped object.
     */
    <T> Optional<T> mapTo(InputStream stream, Class<T> type);

    /**
     * Map a JSON reader to a Java type.
     *
     * @param reader The JSON reader to map.
     * @param type The target type.
     * @return The mapped object.
     */
    <T> Optional<T> mapTo(Reader reader, Class<T> type);

    /**
     * Serialize the given object to JSON.
     *
     * @param obj The object to serialize.
     * @return The JSON representation of {@code obj}.
     */
    Optional<String> stringify(Object obj);
}
