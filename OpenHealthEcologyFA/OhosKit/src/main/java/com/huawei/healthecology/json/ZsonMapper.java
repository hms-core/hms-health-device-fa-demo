/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.healthecology.json;

import lombok.AllArgsConstructor;
import ohos.utils.zson.ZSONObject;

import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Optional;

/**
 * Zson mapper
 */
@AllArgsConstructor(staticName = "get")
public class ZsonMapper implements JsonMapper {
    @Override
    @Deprecated
    public <T> Optional<T> mapTo(String str, Class<T> type) {
        return Optional.ofNullable(ZSONObject.stringToClass(str, type));
    }

    @Override
    @Deprecated
    public Optional<Map<String, String>> getJsonMap(String string) {
        return Optional.empty();
    }

    @Override
    @Deprecated
    public <T> Optional<T> mapTo(InputStream stream, Class<T> type) {
        return Optional.empty();
    }

    @Override
    @Deprecated
    public <T> Optional<T> mapTo(Reader reader, Class<T> type) {
        return Optional.empty();
    }

    @Override
    @Deprecated
    public Optional<String> stringify(Object obj) {
        return Optional.ofNullable(ZSONObject.toZSONString(obj));
    }
}
