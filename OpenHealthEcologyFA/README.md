# OpenHealthEcologyFA
![License](https://img.shields.io/badge/License-Apache-brightgreen)

OpenHealthEcologyFA provides a comprehensive HarmonyOS Connect atomic service template for developers with various Ble and Bluetooth APIs, aiming at health and sport devices.

## Table of Contents

* [Introduction](#introduction)
* [Usage](#usage)
* [Maintainers](#maintainers)
* [Contributing](#contributing)
* [License](#license)
* [Links](#links)

## Introduction

In 2019, Huawei released the concept of HarmonyOS. The following September, HarmonyOS was officially launched. As an important part of HarmonyOS, HarmonyOS Connect enables third-party devices to quickly access, use, and interoperate. Atomic services are important carriers of HarmonyOS Connect on mobile phones. After atomic services are started through NFC touch, proximity detection, and code scanning, you can quickly control devices, obtain data, and provide suggestions. HarmonyOS Connect has comprehensive design specifications and requirements for the process, interface, and interaction of atomized services. Based on the HarmonyOS Connect atomic service, developers can provide users with unified and easy-to-use device control services. On this basis, we developed and launched the HarmonyOS Connect atomic service (Bluetooth Ble direct-connect device) access template to reduce the initial investment of developers and achieve rapid access and commercial use.


## Usage

This project is a ready-to-use template. All you need is to configure few fields before get it work.

### Requirements

Please have the following setup ready:

- [DevEco Studio 2.1 Release or later](https://developer.harmonyos.com/cn/develop/deveco-studio)

- A HarmonyOS device with OS version 2.0.0.200 or later

- A Ble peripheral device(or possibly a phone acting as a Ble peripheral device with the nrf-connect app)

- To join the HarmonyOS Connect, you need to create a product in [DevicePartner](https://devicepartner.huawei.com/cn/console), see [DevicePartner Documentation](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/join-us-0000001134281535)

- You need to create the HarmonyOS application and apply for a certificate in [AppGalleryConnect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/). For details, see [Preparations](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-development-preparations-0000001172144685)


### Configurations

Enter the OpenHealthEcologyFA with DevEco Studio and perform the following operations:

1. Use your own bundleName and packageName

   - Replace the package name in entry：

     Use Refactor -> Rename to change the last group of package name. For example, you can change the package name com.huawei.health.ecology.fa to com.huawei.health.ecology.xxx.

     Alternatively, you can use Refactor > Move Package to change the package name. For example, you can change the package name from com.huawei.health.ecology.fa to xxx.xxx.fa.

   - Replace `bundleName` in entry/src/main/config.json:

     ```json
     {
       "app": {
         "bundleName": "com.huawei.health.ecology.fa",
         ......
       },
     }
     ```

2. Use your own clientId

    Please replace the clientId in `OpenHealthEcologyFA\entry\src\main\config.json`：

    ```json
    "module": {
     "metaData": {
       "customizeData": [
         {
           "name": "com.huawei.hms.client.appid",
           "value": "123456789" // Replace with corresponding OAuth 2.0 clientId
         }
       ]
     },
    ```

    Please set your `clientId`, `clientSecret` and `redirectUri` in `OpenHealthEcologyFA\entry\src\main\js\share\common\config.js`. For details about how to create an application and set a redirect url, see [Applying for Services and Permissions](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-applying-for-health-kit-0000001126264886).:
    ```javascript
    clientId: '123456789',
    clientSecret: '1234567812345678123456781234567812345678123456781234567812345678',
    redirectUri: 'http://127.0.0.1',
    ```

2. Debug with your Ble device

    Replace the values of `customDefaultData` in `\entry\src\main\js\share\common\config.js`:

    `deviceMac` is the default MAC address for debugging, please replace with your own device's address. `productId` is the default ID of the product provided by [Device Partner](https://devicepartner.huawei.com/console/shelve#/). The corresponding resources file should also be modifeid, see [Debugging and Verification ](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis/示例代码使用指南?sort_id=4639203#调测验证).

    ```javascript
    customDefaultData: {
      productId: 'A001', 
      deviceMac: '11:22:33:44:55:66'
    },
    ```

3. Configure App Signature Information

    Click File - Project Structure - Modules - Signing Configs to add your app signature information. Select entry and run the project. You can also make the desktop icon visible to debug more conveniently.

    For details about how to configure app signature and display the desktop icon, see [Debugging and Verification ](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis/示例代码使用指南?sort_id=4639203#调测验证).

## Maintainers

[@Lan Kankan](https://gitee.com/lkkcom)
[@Zhu Haoyu](https://gitee.com/zhuhao0yu)

## Contributing

You can simply report issues, bugs, or functional requirements that you have identified in project Issues.
You can also submit a Pull Request to incorporate your patch, we will review it as needed.

## License

This project uses the [Apache2.0 license](https://www.apache.org/licenses/LICENSE-2.0)。

## Links

[Development Guide for Fitness&Health Devices Atomic Service](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-introduction-0000001172144689)

[Project Wiki](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis)

[HarmonyOS Connect JS Api](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-framework-file-0000000000611396)