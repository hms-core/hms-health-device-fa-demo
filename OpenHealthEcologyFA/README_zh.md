# OpenHealthEcologyFA
![License](https://img.shields.io/badge/License-Apache-brightgreen)

OpenHealthEcologyFA 是华为所提供的专用于运动健康品类三方设备接入HarmonyOS Connect的代码模板。开发者可以通过此模板体验鸿蒙智联设备的快速接入，打造万物互联的智能生态。

## 目录

* [项目简介](#项目简介)
* [用法](#用法)
* [维护者](#维护者)
* [贡献方式](#贡献方式)
* [开源协议](#开源协议)
* [另请参阅](#另请参阅)

## 项目简介

2019年， 华为发布了HarmonyOS相关概念，次年九月，HarmonyOS正式面世。作为HarmonyOS 的重要组成部分，HarmonyOS Connect承载了三方设备的快速接入、快速使用、互联互通的任务。HarmonyOS Connect在手机侧的重要载体为原子化服务，通过NFC碰一碰、靠近发现、扫码等形式拉起原子化服务后，即可快速控制设备，获取数据并给出建议。HarmonyOS Connect对原子化服务对流程、界面、交互均有完善的设计规范与要求。基于HarmonyOS Connect原子化服务，开发者可以为用户提供风格统一，轻松上手的设备控制服务。在此基础上，我们制作并推出了HarmonyOS Connect原子化服务（蓝牙Ble直连类设备）接入模板，以降低开发者的前期投入，实现快速接入，快速商用。



## 用法

此项目提供的模板代码是开箱即用的，您只需对工程中进行少量配置，便可运行模板。

### 准备

请确认您已准备好以下的环境或工具：

- [DevEco Studio 2.1 Release 或更高版本](https://developer.harmonyos.com/cn/develop/deveco-studio)

- 一台鸿蒙手机，软件版本2.0.0.200及以上

- 一台支持蓝牙Ble连接的设备，可以为外设或手机（需使用nrf-connect模拟Ble设备）

- 设备若需要申请鸿蒙智联认证，还需要在[DevicePartner](https://devicepartner.huawei.com/cn/console)创建产品，请参阅[华为智能硬件合作伙伴](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/join-us-0000001134281535)

- 安装示例代码到真机上运行还需要在[AppGalleryConnect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/)创建HarmonyOS应用并申请证书，请参阅[开发准备](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-development-preparations-0000001172144685)


### 配置选项

请在OpenHealthEcologyFA根目录点击右键，选择Open Folder as DevEco Studio Projects，并进行下列操作：

1. 使用您自己项目的bundleName和packageName
   - 修改entry模块下的包名为您创建项目时使用的包名：

     使用Refactor -> Rename更改包名最后一组信息，如可以将包名com.huawei.health.ecology.fa更改为com.huawei.health.ecology.xxx

     或使用Refactor -> Move Package更改包名前的信息，如可以将包名com.huawei.health.ecology.fa更改为xxx.xxx.xxx.fa

   - 修改entry/src/main/config.json中的`bundleName`：

     ```json
     {
       "app": {
         "bundleName": "com.huawei.health.ecology.fa",
         ......
       },
     }
     ```


2. 使用您自己项目的ClientId

   请将`OpenHealthEcologyFA\entry\src\main\config.json`目录下的如下配置进行替换：

    ```json
   "module": {
    "metaData": {
      "customizeData": [
        {
          "name": "com.huawei.hms.client.appid",
          "value": "123456789" // 替换为对应的OAuth 2.0 clientId
        }
      ]
    },
    ```

    请将`OpenHealthEcologyFA\entry\src\main\js\share\common\config.js`目录下的`clientId`，`clientSecret`，`redirectUri`替换为您在华为AppGallery Connect中所创建鸿蒙应用的clientId、ClientSecret、redirectUri（回调地址）等，有关如何创建应用、设置回调地址，请参阅[申请服务和权限](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-applying-for-health-kit-0000001126264886)。
    ```javascript
   clientId: '123456789',
   clientSecret: '1234567812345678123456781234567812345678123456781234567812345678',
   redirectUri: 'http://127.0.0.1',
    ```

3. 使用您自己的蓝牙设备进行调试

    请将` OpenHealthEcologyFA\entry\src\main\js\share\common\config.js`目录下的`customDefaultData`中的相关字段替换为您的设备信息：

    其中，请您将默认的MAC地址`deviceMac`替换为您的蓝牙Ble设备地址。`productId`为默认的调试设备的ProductID，此字段由华为 [Device Partner](https://devicepartner.huawei.com/console/shelve#/)平台为注册设备分配。更换ProductID字段后，还需要更改对应的资源文件，请参阅Wiki[调测验证](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis/示例代码使用指南?sort_id=4639203#调测验证)。

    ```javascript
    customDefaultData: {
      productId: 'A001', 
      deviceMac: '11:22:33:44:55:66'
    },
    ```

4. 配置您的应用签名

    请点击 File - Project Structure - Modules - Signing Configs，配置项目签名。配置好签名后，您可以直接点击右上角的绿色三角运行，请注意在左侧的运行模块中选择entry。为了方便调试，您还可以设置打开桌面图标的显示。

    关于签名文件配置、打开桌面图标等调测相关问题请参阅Wiki[调测验证](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis/示例代码使用指南?sort_id=4639203#调测验证)。

## 维护者

[@Lan Kankan](https://gitee.com/lkkcom)
[@Zhu Haoyu](https://gitee.com/zhuhao0yu)

## 贡献方式

您可以在Issue中提出您检视出的代码问题、bug、或提出您的功能需求。
您还可以提出一个Pull Request将您的补丁包合入，我们会视情况审核。

## 开源协议

本项目采用[Apache2.0协议](https://www.apache.org/licenses/LICENSE-2.0)。

## 另请参阅

[运动健康设备原子化服务开发指南](https://device.harmonyos.com/cn/docs/devicepartner/DevicePartner-Guides/health-fa-introduction-0000001172144689)

[项目Wiki](https://gitee.com/hms-core/hms-health-device-fa-demo/wikis)

[HarmonyOS Connect JS Api](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-framework-file-0000000000611396)