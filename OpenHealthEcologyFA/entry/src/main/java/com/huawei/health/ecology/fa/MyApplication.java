/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa;

import com.huawei.healthecology.module.ClientModule;
import com.huawei.hms.jsb.adapter.har.bridge.HmsBridge;

import ohos.aafwk.ability.AbilityPackage;

/**
 * My application
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        HmsBridge hmsBridge = HmsBridge.getInstance();
        if (hmsBridge != null) {
            hmsBridge.initBridge(this);
        }
        ClientModule.inject(getContext());
        super.onInitialize();
    }

    /**
     * app end
     * When the app ends, log out of the settings.
     */
    @Override
    public void onEnd() {
        HmsBridge hmsBridge = HmsBridge.getInstance();
        if (hmsBridge != null) {
            hmsBridge.destoryBridge();
        }
        ClientModule.destroy();
        super.onEnd();
    }
}
