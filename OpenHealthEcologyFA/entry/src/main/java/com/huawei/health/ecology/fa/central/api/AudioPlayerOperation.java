/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.central.api;

import com.huawei.health.ecology.fa.central.request.AudioPlayRequest;
import com.huawei.health.ecology.fa.central.response.CentralResponseCode;

/**
 * Audio Player Operation interface
 */
public interface AudioPlayerOperation {
    /**
     * To play the list of audio
     *
     * @param audioPlayRequest The audio play request
     * @return the central response code
     */
    CentralResponseCode playAudioList(AudioPlayRequest audioPlayRequest);

    /**
     * To stop the audio playing task
     *
     * @return the central response code
     */
    CentralResponseCode stopAudioPlay();
}