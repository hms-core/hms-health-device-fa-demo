/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.central.api;

import com.huawei.health.ecology.fa.central.callback.PermissionStateCallback;
import com.huawei.health.ecology.fa.central.request.PermissionAcquireRequest;
import com.huawei.health.ecology.fa.central.request.PermissionVerifyRequest;
import com.huawei.health.ecology.fa.central.response.CentralResponseCode;
import com.huawei.health.ecology.fa.central.response.PermissionStateResponse;

/**
 * Ohos Permission Operation interface
 */
public interface OhosPermissionOperation {
    /**
     * Check whether the permission is granted. returned with the callback
     *
     * @param permissionVerifyRequest the permissions to be verified
     * @return the permission state response
     */
    PermissionStateResponse checkPermissions(PermissionVerifyRequest permissionVerifyRequest);

    /**
     * Applying for Permissions from Users
     *
     * @param permissionStateCallback permission callback
     */
    void onPermissionRequest(PermissionStateCallback permissionStateCallback);

    /**
     * Applying for Permissions from Users
     *
     * @param permissionAcquireRequest the permissions to be acquired
     * @return the central response code
     */
    CentralResponseCode requestPermissions(PermissionAcquireRequest permissionAcquireRequest);
}