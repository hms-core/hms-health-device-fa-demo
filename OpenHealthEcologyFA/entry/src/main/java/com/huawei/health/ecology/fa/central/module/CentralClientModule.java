/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.central.module;

import com.huawei.health.ecology.fa.central.processor.AudioPlayerProcessor;
import com.huawei.health.ecology.fa.central.processor.OhosPermissionProcessor;
import com.huawei.healthecology.json.GsonMapper;
import com.huawei.healthecology.json.JsonMapper;
import com.huawei.healthecology.json.JsonMapperType;

import ohos.app.Context;

import java.util.Optional;

/**
 * Central Client module
 */
public class CentralClientModule {
    private static JsonModule jsonModule;

    private static OhosPermissionProcessor ohosPermissionProcessor;

    private static AudioPlayerProcessor audioPlayerProcessor;

    private CentralClientModule() {
    }

    /**
     * Inject context
     *
     * @param context Context
     */
    public static void inject(Context context) {
        jsonModule = JsonModule.init(GsonMapper.get());
        ohosPermissionProcessor = OhosPermissionProcessor.builder().context(context).build();
        audioPlayerProcessor = AudioPlayerProcessor.builder(context).build();
        ohosPermissionProcessor.initProcessor();
        audioPlayerProcessor.initProcessor();
    }

    /**
     * Gets parser.
     *
     * @return The parser
     */
    public static Optional<JsonMapper> getParser() {
        return Optional.ofNullable(jsonModule).map(JsonModule::getMapper);
    }

    /**
     * Gets ohos permission processor.
     *
     * @return The ohos permission processor
     */
    public static Optional<OhosPermissionProcessor> getOhosPermissionProcessor() {
        return Optional.ofNullable(ohosPermissionProcessor);
    }

    /**
     * Gets audio player processor.
     *
     * @return The audio player processor
     */
    public static Optional<AudioPlayerProcessor> getAudioPlayerProcessor() {
        return Optional.ofNullable(audioPlayerProcessor);
    }

    /**
     * Config json mapper
     *
     * @param mapperType Json mapper type
     */
    public static void configJsonMapper(JsonMapperType mapperType) {
        jsonModule = Optional.ofNullable(mapperType)
            .map(type -> JsonModule.init(type.getMapper()))
            .orElse(JsonModule.init(GsonMapper.get()));
    }

    /**
     * Destroy allocations
     */
    public static void destroy() {
        ohosPermissionProcessor.destroyProcessor();
        audioPlayerProcessor.destroyProcessor();
        jsonModule = null;
        ohosPermissionProcessor = null;
        audioPlayerProcessor = null;
    }
}
