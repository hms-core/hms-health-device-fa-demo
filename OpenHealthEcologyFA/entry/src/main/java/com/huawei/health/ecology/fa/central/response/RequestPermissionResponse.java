/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.central.response;

import com.huawei.health.ecology.fa.central.data.PermissionState;

import com.huawei.healthecology.data.HealthEcologyResponse;
import lombok.Builder;
import lombok.Getter;

/**
 * The response code for request permission state
 */
@Getter
public class RequestPermissionResponse extends HealthEcologyResponse<CentralResponseCode> {
    private final int requestCode;

    private final PermissionState permissionState;

    /**
     * Request Permission Response
     *
     * @param centralResponseCode the central response code
     * @param permissionState the permission state
     * @param requestCode Corresponding request code
     */
    @Builder
    public RequestPermissionResponse(CentralResponseCode centralResponseCode, PermissionState permissionState,
        int requestCode) {
        super(centralResponseCode);
        this.requestCode = requestCode;
        this.permissionState = permissionState;
    }
}
