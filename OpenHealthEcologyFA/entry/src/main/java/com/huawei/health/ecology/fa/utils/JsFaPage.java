/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.utils;

/**
 * Presents the Js FA page
 */
public enum JsFaPage {
    HINTS_PAGE ("hints"),

    CONNECTION_PAGE ("connection"),

    DASHBOARD_PAGE ("dashboard")
    ;
    private static JsFaPage currentPage;

    private final String pageName;

    JsFaPage(String value) {
        this.pageName = value;
    }

    public static void setCurrentPage(JsFaPage page) {
        currentPage = page;
    }

    /**
     * The constant name value of current page
     *
     * @return The page name
     */
    public static String getCurrentPage() {
        return currentPage.pageName;
    }
}
