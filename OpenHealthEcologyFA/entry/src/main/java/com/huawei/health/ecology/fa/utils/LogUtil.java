/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log Class for HealthEcology Atomic Service Template
 */
public class LogUtil {
    private static final String GLOBAL_TAG = "HealthEcologyFATemplateLog_";

    private LogUtil() {
    }

    private static HiLogLabel getHiLogLabel(String tag) {
        return new HiLogLabel(0, 0, GLOBAL_TAG + tag);
    }

    /**
     * Send log message.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @return log
     */
    public static int fatal(String tag, String msg) {
        return HiLog.fatal(getHiLogLabel(tag), msg);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     * @return log
     */
    public static int fatal(String tag, String msg, Throwable tr) {
        return HiLog.fatal(getHiLogLabel(tag), msg, tr);
    }

    /**
     * Send log message.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @return log
     */
    public static int debug(String tag, String msg) {
        return HiLog.debug(getHiLogLabel(tag), msg);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     * @return log
     */
    public static int debug(String tag, String msg, Throwable tr) {
        return HiLog.debug(getHiLogLabel(tag), msg, tr);
    }

    /**
     * Send an log message.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @return log
     */
    public static int info(String tag, String msg) {
        return HiLog.info(getHiLogLabel(tag), msg);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     * @return log
     */
    public static int info(String tag, String msg, Throwable tr) {
        return HiLog.info(getHiLogLabel(tag), msg, tr);
    }

    /**
     * Send log message.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @return log
     */
    public static int warn(String tag, String msg) {
        return HiLog.warn(getHiLogLabel(tag), msg);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     * @return log
     */
    public static int warn(String tag, String msg, Throwable tr) {
        return HiLog.warn(getHiLogLabel(tag), msg, tr);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param tr An exception to log
     * @return log
     */
    public static int warn(String tag, Throwable tr) {
        return HiLog.warn(getHiLogLabel(tag), "", tr);
    }

    /**
     * Send log message.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @return log
     */
    public static int error(String tag, String msg) {
        return HiLog.error(getHiLogLabel(tag), msg);
    }

    /**
     * Send log message and log the exception.
     *
     * @param tag Used to identify the source of log message.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     * @return log
     */
    public static int error(String tag, String msg, Throwable tr) {
        return HiLog.error(getHiLogLabel(tag), msg, tr);
    }
}
