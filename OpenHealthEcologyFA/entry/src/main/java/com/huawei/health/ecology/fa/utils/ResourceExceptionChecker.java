/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.utils;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * A check tool to throws resource acquisition exceptions
 *
 * @param <T> the type of input parameter
 * @param <R> the type of return value
 */
@FunctionalInterface
public interface ResourceExceptionChecker<T, R> {
    /**
     * Apply the function and throws the corresponding exception.
     *
     * @param inputParameter the input parameter
     * @return the result of apply function
     * @throws NotExistException the NotExistException
     * @throws WrongTypeException the WrongTypeException
     * @throws IOException the IO exception
     */
    R apply(T inputParameter) throws NotExistException, WrongTypeException, IOException;
}
