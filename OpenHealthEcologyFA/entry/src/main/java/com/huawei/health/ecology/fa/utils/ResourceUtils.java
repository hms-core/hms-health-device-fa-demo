/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.health.ecology.fa.utils;

import ohos.app.Context;
import ohos.global.resource.Element;

import java.util.Optional;

/**
 * To acquire ohos resources
 */
public class ResourceUtils {
    /**
     * getString
     *
     * @param context context
     * @param resourceId resourceId
     * @return string resource
     */
    public static String getString(Context context, int resourceId) {
        return Optional.ofNullable(context)
            .map(Context::getResourceManager)
            .map(OhosResourceAttempt.apply(resourceManager -> resourceManager.getElement(resourceId)))
            .map(OhosResourceAttempt.apply(Element::getString))
            .orElse(null);
    }

    /**
     * getColor
     *
     * @param context context
     * @param resourceId resourceId
     * @return int color
     */
    public static int getColor(Context context, int resourceId) {
        return Optional.ofNullable(context)
            .map(Context::getResourceManager)
            .map(OhosResourceAttempt.apply(resourceManager -> resourceManager.getElement(resourceId)))
            .map(OhosResourceAttempt.apply(Element::getColor))
            .orElse(0);
    }
}
