/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import app from '@system.app';
import state from '../../../../share/common/state.js'

const showState = [
    state.connection.CONNECT_FAILED,
    state.connection.DISCOVER_FAILED,
    state.connection.RECONNECT_REQUESTED,
    state.connection.REDISCOVER_REQUESTED,
    state.connection.PERMISSION_DENIED]
const JS_TAG = 'JS/Component/Connect failed dialog: ';

export default {
  props: ['state', 'productId'],
  data: {
    toShowHelpTips: true,
    boxChecked: false
  },
  computed: {
    showObject() {
      return showState.indexOf(this.state) > -1;
    },
    deviceResource() {
      return this.$t('resources.' + this.productId);
    },
    promptText() {
      if (this.state == state.connection.CONNECT_FAILED || this.state ==state.connection.RECONNECT_REQUESTED){
        return this.$t('strings.public_connection_failed');
      } else {
        return this.$t('strings.public_discovery_failed');
      }
    }
  },
  onInit() {
    console.debug(JS_TAG + 'onInit');
  },
  exit() {
    app.terminate();
  },
  helpButtonClick() {
    this.toShowHelpTips = !this.toShowHelpTips;
  },
  reConnect() {
    this.$emit('onReConnect');
    this.toShowHelpTips = !this.toShowHelpTips;
  },
  onCheckboxChange(status) {
    console.debug(JS_TAG + ' onCheckboxChange start');
    this.boxChecked = status.checked;
  },
  hideHelpDetails() {
    this.toShowHelpTips = true;
  }
}
