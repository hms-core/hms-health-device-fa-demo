/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
  props: ['productId'],
  data: {
    items: [],
    itemsLength: 0,
    boxChecked: false,
  },
  computed: {
    checkboxIcon() {
      return this.boxChecked ? 'common/img/base/ic_public_checked.png' : 'common/img/base/ic_public_unchecked.png'
    }
  },
  onInit() {
    let helpItems = this.$t('resources.' + this.productId).connect_help_items;
    if (helpItems && helpItems.length > 0) {
      this.items = helpItems;
    }
    this.itemsLength = this.items.length;
    this.boxChecked = !(this.itemsLength > 1);
  },
  onCheckboxChange() {
    this.boxChecked = !this.boxChecked;
  },
  cancel() {
    this.$emit('cancel');
  },
  reConnect() {
    this.$emit('reConnect');
  }
}