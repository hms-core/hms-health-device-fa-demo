/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import app from '@system.app';
import prompt from '@system.prompt';
import config from '../../../../share/common/config.js'
import state from '../../../../share/common/state.js'
import HuaweiAccount from '../../../../../js/share/common/scripts/HuaweiAccount';

const showState = [state.connection.PRIVACY]
const JS_TAG = 'JS/Component/Login dialog: ';

export default {
  props: ['state'],
  data: {
    checked: false,
    version: '',
    hwLogo: '',
  },
  computed: {
    showObject() {
      return showState.indexOf(this.state) > -1;
    },
    checkboxIcon() {
      return this.checked ? 'common/img/base/ic_public_checked.png' : 'common/img/base/ic_public_unchecked.png'
    }
  },
  onInit() {
    console.debug(JS_TAG + 'onInit');
    this.version = app.getInfo().versionName;
  },
  onReady() {
    if (this.$app.$def.globalData.isDarkMode) {
      this.hwLogo = 'common/img/base/ic_public_hw_logo_dark.png'
    } else {
      this.hwLogo = 'common/img/base/ic_public_hw_logo.png'
    }
  },
  login() {
    HuaweiAccount.signIn().then((loginResult) => {
      this.$emit('onLoginSuccess', loginResult)
    }, this.processLoginError);
  },
  processLoginError(loginError) {
    if (loginError.showMsg) {
      prompt.showToast({
        message: this.$t(loginError.showMsg),
        duration: config.toastDuration
      });
    } else if (loginError.error_description) {
      prompt.showToast({
        message: loginError.error_description,
        duration: config.toastDuration
      });
    } else {
      console.error(JS_TAG + 'processLoginError, error = ' + JSON.stringify(loginError));
    }
    this.$emit('onLoginError', loginError);
  },
  onCheckboxChange() {
    this.checked = !this.checked;
  },
  openPrivacyDialog() {
    console.info(JS_TAG + 'openPrivacyDialog');
    this.$emit('onPrivacyDisplay');
  },
  exit() {
    app.terminate();
  },
}