/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import app from '@system.app';
import state from '../../../../share/common/state.js'

const showState = [state.connection.PRIVACY_CONTENT]
const JS_TAG = 'JS/Component/privacy content dialog: ';
export default {
  props:['state'],
  computed:{
    showObject(){
      return showState.indexOf(this.state) > -1;
    },
    contentArray(){
      return this.$t('resources.content_array');
    },
  },
  onInit() {
    console.debug(JS_TAG + 'onInit');
  },
  onPrivacyConfirmed(){
    this.$emit('onPrivacyConfirmed');
  },
}
