/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const JS_TAG = 'JS/App/Dashboard: ';

export default {
  globalData: {
    productId: '',
    macAddress: '',
    isFullScreen: false,
    isDarkMode: false,
    accessToken: '',
    accessTokenExpireTimestamp: 0,
    bundleName:''
  },
  onCreate() {
    console.info(JS_TAG + 'AceApplication onCreate');
  },
  onDestroy() {
    console.info(JS_TAG + 'AceApplication onDestroy, closing Ble: ' + this.globalData.macAddress);
  }
};
