/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router'
import TitleBarParams from '../../../share/common/components/titleBar/TitleBarParams';

const JS_TAG = 'JS/Page/Dashboard/guide: ';

export default {
  data: {
    deviceResource:undefined
  },
  onInit() {
    console.debug(JS_TAG + 'this.$app.$def.globalData.productId = ' + this.$app.$def.globalData.productId);
    this.deviceResource = this.$t('resources.' + this.$app.$def.globalData.productId);
    this.titleBarConfig= TitleBarParams.getTitleBarConfig(
      {
        title:this.$t('strings.public_guide'),
      });
  },
  back: function () {
    router.back();
  }
}