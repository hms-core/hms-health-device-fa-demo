/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';
import TitleBarParams from '../../../share/common/components/titleBar/TitleBarParams';
import DefaultValueOperator from '../../../share/common/scripts/defaultValueOperator.js';

const DEFAULT_IMAGE_PATH = '/common/img/base/ic_public_arrow_right.png';

export default {
  data: {
    listHeight: 0,
    settingItems: [],
    arrowRightImagePath: DEFAULT_IMAGE_PATH
  },
  onInit() {
    let productId = this.$app.$def.globalData.productId;
    let items = this.$t('resources.' + productId).setting_items;
    if (items) {
      this.settingItems = items;
    }
    this.arrowRightImagePath =
    DefaultValueOperator.imgPath(this.arrowRightImagePath, DEFAULT_IMAGE_PATH, this.$app.$def.globalData.isDarkMode);

    this.initTitleBar();
    this.computedListHeight();
  },
  initTitleBar() {
    this.titleBarParams = TitleBarParams.getTitleBarConfig({
      title: this.$t('strings.public_settings')
    })
  },
  computedListHeight() {
    this.listHeight = this.settingItems.reduce((height) => {
      return height += 50;
    }, 0);
  },
  clickItem(value) {
    if (value.router && value.router.uri) {
      router.push({
        uri: value.router.uri,
      });
    }
  },
}