/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import AdjusterButtonParams from './AdjusterButtonParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Adjuster Button : '
const ICON_LEFT_INACTIVE = '/common/img/base/ic_public_remove_inactive.png'
const ICON_LEFT_ACTIVE = '/common/img/base/ic_public_remove_active.png'
const ICON_RIGHT_INACTIVE = '/common/img/base/ic_public_add_norm_inactive.png'
const ICON_RIGHT_ACTIVE = '/common/img/base/ic_public_add_norm_active.png'

export default {
  props: ['config', 'data'],
  data: {
    title: 'strings.adjuster_button.default_title',
    leftIcon: ICON_LEFT_INACTIVE,
    rightIcon: ICON_RIGHT_INACTIVE,
    defaultValue: 0,
  },
  computed: {
    isMinGears() {
      return this.defaultValue == this.minGears
    },
    isMaxGears() {
      return this.defaultValue == this.maxGears
    },
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let parsedConfig = AdjusterButtonParams.getAdjusterButtonConfig(this.config)
    this.title = this.$t(DefaultValueOperator.stringValue(parsedConfig.title, this.title))
    this.subTitle = this.$t(DefaultValueOperator.stringValue(parsedConfig.subTitle, this.subTitle))
    this.minGears = DefaultValueOperator.numberValue(parsedConfig.minGears, this.minGears)
    this.maxGears = DefaultValueOperator.numberValue(parsedConfig.maxGears, this.maxGears)
    this.step = DefaultValueOperator.numberValue(parsedConfig.step, this.step)
    this.leftIcon = DefaultValueOperator.imgPath(this.leftIcon, ICON_LEFT_INACTIVE, this.$app.$def.globalData.isDarkMode);
    this.rightIcon = DefaultValueOperator.imgPath(this.rightIcon, ICON_RIGHT_INACTIVE, this.$app.$def.globalData.isDarkMode);
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = AdjusterButtonParams.getAdjusterButtonData(this.data)
    this.defaultValue = DefaultValueOperator.numberValue(parsedData.defaultValue, this.defaultValue)
  },
  leftTouchStart() {
    this.leftIcon = DefaultValueOperator.imgPath(ICON_LEFT_ACTIVE, ICON_LEFT_INACTIVE, this.$app.$def.globalData.isDarkMode);
  },
  leftTouchEnd() {
    this.leftIcon = DefaultValueOperator.imgPath(ICON_LEFT_INACTIVE, ICON_LEFT_INACTIVE, this.$app.$def.globalData.isDarkMode);
  },
  rightTouchStart() {
    this.rightIcon = DefaultValueOperator.imgPath(ICON_RIGHT_ACTIVE, ICON_RIGHT_ACTIVE, this.$app.$def.globalData.isDarkMode);
  },
  rightTouchEnd() {
    this.rightIcon = DefaultValueOperator.imgPath(ICON_RIGHT_INACTIVE, ICON_RIGHT_INACTIVE, this.$app.$def.globalData.isDarkMode);
  },
  onUp() {
    if ((this.defaultValue + this.step) > this.maxGears) {
      return
    }
    this.defaultValue = this.defaultValue + this.step
    this.onChange()
  },
  onDown() {
    if ((this.defaultValue - this.step) < this.minGears) {
      return
    }
    this.defaultValue = this.defaultValue - this.step
    this.onChange()
  },
  onChange() {
    this.$emit('onChange', {
      value: this.defaultValue
    })
  }
}