/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Exception Hint: ';
const DEFAULT_IMAGE_PATH = '/common/img/base/ic_public_arrow_right.png';

export default {
  data: {
    arrowRightImagePath: DEFAULT_IMAGE_PATH
  },
  props: {
    showObject: {
      default: false,
    },
    label: {
      default: '',
    },
    icon: {
      default: '/common/img/base/ic_public_fail_error.png'
    },
    bgColor: {
      default: '#0De84026'
    },
    labelColor: {
      default: '#e84026'
    },
    showNextIcon: {
      default: false
    }
  },
  onInit() {
    console.debug(JS_TAG + 'onInit');
    this.arrowRightImagePath =
      DefaultValueOperator.imgPath(this.arrowRightImagePath, DEFAULT_IMAGE_PATH, this.$app.$def.globalData.isDarkMode);
  },
  onClick() {
    this.$emit('onPressed');
  }
}