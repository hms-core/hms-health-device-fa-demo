/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const LOAD_PROGRESS=1;
const PERCENT_PROGRESS=2;
export default {
  props: ['progressStatus', 'percent', 'updateVersion', 'productLogoIcon'],
  data: {
    isProgress: true,
    isClock: false,
    updatePercent: 0,
    updateText: '0%',
    updateVersion: '1.0.0',
    percentShow: false,
    imageShow: true,
  },
  onInit() {
    this.$watch('progressStatus', 'statusChange')
    this.$watch('percent', 'percentChange')
  },
  onPageShow() {
    this.onShow();
  },
  statusChange(progressStatus, oldV) {
    if (progressStatus == LOAD_PROGRESS) {
      this.updatePercent = 20;
      this.start();
      var context = this;
    }
    if (progressStatus == PERCENT_PROGRESS) {
      this.imageShow = false;
      this.percentShow = true;
      this.finish();
    }
  },
  percentChange(newV, oldV) {
    if (!this.isClock) {
      this.isClock = true;
    }
    this.updateText = newV + '%';
    this.updatePercent = newV;
  },
  onShow() {
    var options = {
      duration: 1600,
      easing: 'linear',
      fill: 'forwards',
      iterations: 'Infinity',
      direction: 'normal',
    };
    var frames = [
      {
        transform: {
          rotate: '0'
        }
      },
      {
        transform: {
          rotate: '360'
        }
      },
    ];
    this.animation = this.$element('progress-id').animate(frames, options);
    var context = this;
    this.animation.onfinish = function () {
      context.updatePercent = 0;
    };
    this.animation.onrepeat = function () {
      if (context.isProgress) {
        context.updatePercent = 0;
        context.isProgress = false;
      } else {
        context.updatePercent = 20;
        context.isProgress = true;
      }
    };
  },
  start() {
    this.animation.play();
  },
  cancel() {
    this.animation.cancel();
  },
  finish() {
    this.animation.finish();
  }
}
