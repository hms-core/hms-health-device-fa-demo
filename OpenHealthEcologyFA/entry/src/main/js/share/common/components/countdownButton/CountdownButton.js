/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import CountdownButtonConfig from './CountdownButtonParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Countdown Button : '

export default {
  props: ['config', 'data'],
  data: {
    title: 'strings.countdown_button.default_title',
    inactiveIcon: '/common/img/base/ic_public_clock_inactive.png',
    activeIcon: '/common/img/base/ic_public_clock_active.png',
    defaultValue: undefined,
    defaultSubTitle: '',
    selected: 0,
    pickerRange: [],
    indicatorPrefix:'',
    indicatorSuffix:''
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
  },
  onReady() {
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let parsedConfig = CountdownButtonConfig.getCountdownButtonConfig(this.config)
    this.title = this.$t(DefaultValueOperator.stringValue(parsedConfig.title, this.title))
    this.pickerRange = DefaultValueOperator.arrayValue(parsedConfig.range, this.pickerRange)
    this.indicatorPrefix = this.$t(DefaultValueOperator.stringValue(parsedConfig.indicatorPrefix, this.indicatorPrefix))
    this.indicatorSuffix = this.$t(DefaultValueOperator.stringValue(parsedConfig.indicatorSuffix, this.indicatorSuffix))
    this.inactiveIcon = DefaultValueOperator.imgPath(parsedConfig.inactiveIcon, this.inactiveIcon, this.$app.$def.globalData.isDarkMode);
    this.activeIcon = DefaultValueOperator.imgPath(parsedConfig.activeIcon, this.activeIcon, this.$app.$def.globalData.isDarkMode);
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = CountdownButtonConfig.getCountdownButtonData(this.data)
    this.defaultValue = DefaultValueOperator.numberValue(parsedData.defaultValue, this.defaultValue)
    this.defaultSubTitle = this.$t(DefaultValueOperator.stringValue(parsedData.defaultSubTitle, this.defaultSubTitle))

    this.pickerRange.forEach((value, index) => {
      if (this.defaultValue == value) {
        this.selected = ''
        this.selected = index
      }
    })
  },
  popupPicker() {
    let selectItem = this.selected;
    this.selected = '';
    this.selected = selectItem;
    this.$element('pickerDialog').show();
  },
  textOnChange(changeEvent) {
    this.defaultValue = changeEvent.newValue;
  },
  cancelPickerSchedule() {
    this.$element('pickerDialog').close()
  },
  setPickerSchedule() {
    this.onChange()
    this.$element('pickerDialog').close()
  },
  onChange() {
    if (this.defaultValue == undefined || this.defaultValue == '') {
      this.defaultValue = this.pickerRange[this.selected]
    }
    this.$emit('onChange', {
      value: this.defaultValue
    })
  },
}