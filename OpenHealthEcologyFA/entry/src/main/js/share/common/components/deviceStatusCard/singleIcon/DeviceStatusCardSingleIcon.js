/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import DeviceStatusCardParams from './DeviceStatusCardSingleIconParams'
import DefaultValueOperator from '../../../scripts/defaultValueOperator.js'
import DeviceStateConstants from '../DeviceStateConstants'

const JS_TAG = 'JS/Component/Device Status Card Single Icon: '
const showType = DeviceStateConstants.SINGLE_ICON

export default {
  data: {
    title: '',
    subTitle: '',
    middleTitle: '',
    iconPath: '',
  },
  props: ['type', 'data'],
  computed: {
    showObj() {
      return showType == this.type;
    },
  },
  onInit() {
    this.$watch('data', 'onPropertyChange');
    if (this.data == undefined) {
      return
    }

    let parsedData = DeviceStatusCardParams.getDeviceStatusCardSingleIconData(this.data);
    this.title = DefaultValueOperator.stringValue(parsedData.title, this.title);
    this.subTitle = DefaultValueOperator.stringValue(parsedData.subTitle, this.subTitle);
    this.iconPath = DefaultValueOperator.imgPath(parsedData.iconPath, this.iconPath, this.$app.$def.globalData.isDarkMode);
    this.middleTitle = DefaultValueOperator.stringValue(parsedData.middleTitle, this.middleTitle);
  },
  onPropertyChange() {
    if (this.data == undefined) {
      return
    }
    let parsedData = DeviceStatusCardParams.getDeviceStatusCardSingleIconData(this.data);
    this.title = DefaultValueOperator.stringValue(parsedData.title, this.title);
    this.middleTitle = DefaultValueOperator.stringValue(parsedData.middleTitle, this.middleTitle);
    this.subTitle = DefaultValueOperator.stringValue(parsedData.subTitle, '');
    this.iconPath = DefaultValueOperator.imgPath(parsedData.iconPath, this.iconPath, this.$app.$def.globalData.isDarkMode);
  },
  onReady() {
  },
  onChange() {
    this.$emit('onIconPressed', {});
  }
}