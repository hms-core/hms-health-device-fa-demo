/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

interface ButtonConfig {
    leftButtonLabel ?: String,
    rightButtonLabel ?: String,
    showLeftButton ?: boolean, // 是否显示左button，默认true
    showRightButton ?: boolean, // 是否显示右button，默认true
    leftButtonTextColor ?: String, //左button文字颜色
    rightButtonTextColor ?: String, //右button文字颜色
    leftButtonBgColor ?: String, //左button背景色
    rightButtonBgColor ?: String, //右button背景色
}

export {ButtonConfig}