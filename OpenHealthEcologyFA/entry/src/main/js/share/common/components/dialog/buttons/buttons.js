/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import DefaultValueOperator from '../../../scripts/defaultValueOperator.js';

export default {
  props: ['config'],
  data: {
    leftButtonLabel: 'strings.selectorDialog.left_Button_str',
    rightButtonLabel: 'strings.selectorDialog.right_Button_str',
    showLeftButton: true,
    showRightButton: true,
    leftButtonBgColor: 'rgba(0,0,0,0)',
    leftButtonTextColor: '#0A59F7',
    rightButtonBgColor: '#0A59F7',
    rightButtonTextColor: '#FFFFFF',
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.initConfig()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let buttonConfig = this.config.buttonConfig
    if (buttonConfig == undefined) {
      this.leftButtonLabel = this.$t(this.leftButtonLabel)
      this.rightButtonLabel = this.$t(this.rightButtonLabel)
      return
    }
    this.leftButtonLabel = this.$t(DefaultValueOperator.stringValue(buttonConfig.leftButtonLabel, this.leftButtonLabel))
    this.rightButtonLabel = this.$t(DefaultValueOperator.stringValue(buttonConfig.rightButtonLabel, this.rightButtonLabel))
    this.showLeftButton = DefaultValueOperator.booleanValue(buttonConfig.showLeftButton, this.showLeftButton)
    this.showRightButton = DefaultValueOperator.booleanValue(buttonConfig.showRightButton, this.showRightButton)
    this.leftButtonTextColor = DefaultValueOperator.stringValue(buttonConfig.leftButtonTextColor, this.leftButtonTextColor)
    this.rightButtonTextColor = DefaultValueOperator.stringValue(buttonConfig.rightButtonTextColor, this.rightButtonTextColor)
    this.leftButtonBgColor = DefaultValueOperator.stringValue(buttonConfig.leftButtonBgColor, this.leftButtonBgColor)
    this.rightButtonBgColor = DefaultValueOperator.stringValue(buttonConfig.rightButtonBgColor, this.rightButtonBgColor)
  },
  onClickLeft() {
    this.$emit('onLeftPressed')
  },
  onClickRight() {
    this.$emit('onRightPressed')
  }
}