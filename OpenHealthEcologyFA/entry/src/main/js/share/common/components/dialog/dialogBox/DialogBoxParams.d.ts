/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {TextPickerConfig, TextPickerData} from '../textPicker/textPickerParams'
import {ListSelectorConfig, ListSelectorData} from '../listSelector/listSelectorParams'
import {InfoConfig} from '../info/infoParams'


declare class DialogBoxParams{
    public static getTextPickerConfig(config:TextPickerConfig):TextPickerConfig;
    public static getTextPickerData(data:TextPickerData):TextPickerData;

    public static getListSelectorConfig(config:ListSelectorConfig):ListSelectorConfig;
    public static getListSelectorData(config:ListSelectorData):ListSelectorData;
  
    public static getInfoConfig(config:InfoConfig):InfoConfig;
}

export default DialogBoxParams;