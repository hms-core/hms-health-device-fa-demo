/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const TEXT_PICKER = 0;
const TEXT_LIST = 1;
const INFO = 2;

class DialogBoxParams {
  static getTextPickerConfig(config) {
    config.type = TEXT_PICKER
    return config
  }

  static getTextPickerData(config) {
    return config
  }

  static getListSelectorConfig(config) {
    config.type = TEXT_LIST
    return config
  }

  static getListSelectorData(config) {
    return config
  }

  static getInfoConfig(config) {
    config.type = INFO
    return config
  }
}

export default DialogBoxParams;