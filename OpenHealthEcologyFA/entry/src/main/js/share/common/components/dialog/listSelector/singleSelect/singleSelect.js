/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ITEM_HEIGHT_1, ITEM_HEIGHT_2} from '../listSelector.js';

export default {
  props: ['index', 'value', 'defaultIndex', 'inactiveIcon', 'activeIcon'],
  data: {
    title: '',
    subTitle: '',
    selected: false,
  },
  computed: {
    itemHeight() {
      return this.subTitle ? ITEM_HEIGHT_2 : ITEM_HEIGHT_1;
    },
  },
  onInit() {
    this.$watch('value', 'initValue');
    this.$watch('defaultIndex', 'initDefaultIndex');
    this.initValue();
    this.initDefaultIndex();
  },
  initValue() {
    if (this.value == undefined) {
      return;
    }
    this.title = this.$t(this.value.title);
    this.subTitle = this.$t(this.value.subTitle);
  },
  initDefaultIndex() {
    if (this.defaultIndex == undefined) {
      this.selected = false;
    } else {
      this.selected = this.index == this.defaultIndex[0];
    }
  },
  onClickItem(index) {
    this.$emit('onItemPressed', {
      index: [index],
    });
  },
}