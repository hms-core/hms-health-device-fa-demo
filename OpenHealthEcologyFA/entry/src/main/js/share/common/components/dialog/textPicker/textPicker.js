/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
  props: ['config', 'data'],
  data: {
    items: [],
    indicatorPrefix: '',
    indicatorSuffix: '',
    defaultIndex: 0,
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.config = newV
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.data = newV
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    this.items = this.config.items
    this.indicatorPrefix = this.$t(this.config.indicatorPrefix)
    this.indicatorSuffix = this.$t(this.config.indicatorSuffix)
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    this.defaultIndex = ''
    this.defaultIndex = this.data.defaultIndex
  },
  onChange(changeEvent) {
    this.defaultIndex = changeEvent.newSelected

    this.$emit('onChangeIndex', {
      index: this.defaultIndex
    })
  },
}