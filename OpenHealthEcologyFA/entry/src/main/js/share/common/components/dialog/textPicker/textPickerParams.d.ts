/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {TitleConfig} from '../title/titleParams'
import { ButtonConfig } from '../buttons/buttonParams'

interface TextPickerConfig {
    titleConfig ?: TitleConfig,
    buttonConfig ?: ButtonConfig,
    items : Object[],
    indicatorPrefix ? : String, // 文本选择器选定值增加的前缀字段。
    indicatorSuffix ? : String, // 文本选择器选定值增加的后缀字段。
}

interface TextPickerData {
    defaultIndex ?: Number,// 默认选中索引
}

export {TextPickerConfig, TextPickerData}