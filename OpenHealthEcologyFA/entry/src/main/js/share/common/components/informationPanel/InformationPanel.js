/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import state from '../../state.js'
import InformationPanelParams from './InformationPanelParams'

const JS_TAG = 'JS/Component/Information Panel: ';

export default {
  props: ['config', 'data'],
  computed: {
    resultParams() {
      return this.$t('resources.all_devices.result_card_params')
    }
  },
  data: {
    resultConfigPage1: {},
    resultDataPage1: {},
    resultConfigPage2: {},
    resultDataPage2: {},
    swiperEnabled: false
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let parsedConfig = InformationPanelParams.getInformationPanelConfig(this.config)
    if (parsedConfig.config.length > 3) {
      this.swiperEnabled = true;
      this.resultConfigPage1 = parsedConfig.config.slice(0, 3)
      this.resultConfigPage2 = parsedConfig.config.slice(3)
    } else {
      this.swiperEnabled = false;
      this.resultConfigPage1 = parsedConfig.config;
      this.resultConfigPage2 = undefined
    }
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = InformationPanelParams.getInformationPanelData(this.data)
    if (parsedData.data.length > 3) {
      this.swiperEnabled = true;
      this.resultDataPage1 = parsedData.data.slice(0, 3)
      this.resultDataPage2 = parsedData.data.slice(3)
    } else {
      this.swiperEnabled = false;
      this.resultDataPage1 = parsedData.data;
      this.resultDataPage2 = undefined
    }
  },
}