/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router';
import DefaultValue from '../../../scripts/defaultValueOperator';
import CustomizedInformationPanelParams from './CustomizedInformationPanelParams';

export default {
  props: ['titleConfig', 'router', 'data'],
  data: {
    pageSize: 1,
    pageItems: [],
    column: 3,
    title: '',
    subTitle: '',
    items: [],
  },
  onInit() {
    this.$watch('titleConfig', 'initTitle');
    this.$watch('data', 'initData');
    this.initTitle();
    this.initData();
  },
  initTitle() {
    if (!this.titleConfig) {
      return;
    }
    let parsedTitle = CustomizedInformationPanelParams.getTitleConfig(this.titleConfig);

    if (parsedTitle.title) {
      this.title = this.$t(parsedTitle.title);
    }
    if (parsedTitle.subTitle) {
      this.subTitle = this.$t(parsedTitle.subTitle);
    }
  },
  initData() {
    if (!this.data) {
      return;
    }

    let parsedData = CustomizedInformationPanelParams.getData(this.data);
    this.items = DefaultValue.arrayValue(parsedData.items, []);

    this.pageSize = 1;
    this.pageItems = [];
    this.pageSize = Math.ceil(this.items.length / this.column);
    for (let i = 0; i < this.pageSize; i++) {
      let item = this.items.slice(i * this.column, (i + 1) * this.column);
      while (item.length < this.column) {
        item.push(undefined);
      }
      this.pageItems.push(item);
    }
  },
  jump() {
    if (!this.router) {
      return;
    }
    router.push({
      uri: this.router.uri,
      params: this.router.params
    });
  }
}