/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import CustomizedInformationPanelParams from '../CustomizedInformationPanelParams';

const VALUE_COLOR = ['', '#64BB5C', '#F7CE00', '#E84026']

export default {
  props: ['itemData', 'index'],
  data: {
    type: 0,
    value: '',
    valueColor: '',
    unit: '',
    description: '',
  },
  onInit() {
    this.$watch('itemData', 'initData')
    this.initData()
  },
  initData() {
    if (!this.itemData) {
      return
    }
    let parsedItem = CustomizedInformationPanelParams.getItem(this.itemData)
    if (parsedItem.type) {
      this.type = parsedItem.type
    }
    this.value = this.$t(parsedItem.value)
    this.unit = this.$t(parsedItem.unit)
    this.description = this.$t(parsedItem.description)

    this.valueColor = VALUE_COLOR[this.type]
  },
}