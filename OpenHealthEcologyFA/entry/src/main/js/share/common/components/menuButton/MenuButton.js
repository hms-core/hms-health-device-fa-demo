/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MenuButtonParams from './MenuButtonParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Menu Button : '

export default {
  props: ['config', 'data'],
  data: {
    title: 'strings.menu_button.default_title',
    activeIcon: '/common/img/base/ic_public_sound_active.png',
    inactiveIcon: '/common/img/base/ic_public_sound_inactive.png',
    subTitle: '',
    selectOptions: [],
    defaultIndex: undefined,
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
  },
  onReady() {
    this.initConfig()
    this.initData()
  },
  configUpdate() {
    this.initConfig()
  },
  dataUpdate() {
    this.initData()
  },
  initConfig() {
    let parsedConfig = MenuButtonParams.getMenuButtonConfig(this.config)
    this.title = this.$t(DefaultValueOperator.stringValue(parsedConfig.title, this.title))
    this.activeIcon = DefaultValueOperator.imgPath(parsedConfig.activeIcon, this.activeIcon, this.$app.$def.globalData.isDarkMode)
    this.inactiveIcon = DefaultValueOperator.imgPath(parsedConfig.inactiveIcon, this.inactiveIcon, this.$app.$def.globalData.isDarkMode)
    this.selectOptions = DefaultValueOperator.arrayValue(parsedConfig.selectOptions, this.selectOptions)
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = MenuButtonParams.getMenuButtonData(this.data)
    this.defaultIndex = parsedData.defaultIndex

    if (this.defaultIndex == undefined || this.defaultIndex < 0 || this.defaultIndex >= this.selectOptions.length) {
      return
    }
    this.subTitle = this.selectOptions[this.defaultIndex].describe
  },
  onSelected(changeEvent) {
    this.subTitle = this.selectOptions[changeEvent.newValue].describe
    this.onChange(changeEvent)
  },
  onChange(changeEvent) {
    this.$emit('onChange', {
      value: changeEvent.newValue
    })
  },
}