/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

interface MenuButtonConfig {
    title : string,
    activeIcon ? : string,
    inactiveIcon ? : string,
    selectOptions : SelectOptions[],
}

interface SelectOptions{
    describe : string,
}

interface MenuButtonData{
    defaultIndex : Number,
}

declare class MenuButtonParams{
    public static getMenuButtonConfig(config:MenuButtonConfig):MenuButtonConfig;
    public static getMenuButtonData(data:MenuButtonData):MenuButtonData;
}

export default MenuButtonParams;