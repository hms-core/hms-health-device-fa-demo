/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ModesSwitchPanelParams from './ModesSwitchPanelParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Mode Switch Panel : '

export default {
  props: ['config', 'data'],
  data: {
    items: [],
    defaultMode: 0,
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
  },
  onReady() {
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return;
    }
    let parsedConfig = ModesSwitchPanelParams.getModesSwitchPanelConfig(this.config);
    this.items = DefaultValueOperator.arrayValue(parsedConfig.items, this.items);
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].activeIcon =
        DefaultValueOperator.imgPath(this.items[i].activeIcon, this.items[i].activeIcon, this.$app.$def.globalData.isDarkMode);
      this.items[i].inactiveIcon =
        DefaultValueOperator.imgPath(this.items[i].inactiveIcon, this.items[i].activeIcon, this.$app.$def.globalData.isDarkMode);
    }
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = ModesSwitchPanelParams.getModesSwitchPanelData(this.data)
    this.defaultMode = parsedData.defaultMode
    if (this.defaultMode == undefined && this.items.length > 0) {
      this.defaultMode = this.items[0].mode
    }
  },
  onChange(item) {
    this.defaultMode = item.mode
    this.$emit('onChange', {
      value: item
    })
  }
}