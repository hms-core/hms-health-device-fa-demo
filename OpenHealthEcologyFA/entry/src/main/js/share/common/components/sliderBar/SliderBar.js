/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SliderBarParams from './SliderBarParams'
import DefaultValueOperator from '../../../common/scripts/defaultValueOperator.js';

const JS_TAG = 'JS/Component/Slider Bar: ';

export default {
  props: ['config', 'data'],
  data: {
    title: 'Title',
    subTitle: '',
    showSubTitle: true,
    min: 0,
    max: 100,
    step: 1,
    value: 0,
    unit: '%',
    mode: 'outset',
  },
  onInit() {
    this.subTitle = this.value + this.unit
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
    this.initConfig()
    this.initData()
  },
  configUpdate() {
    this.initConfig()
  },
  dataUpdate() {
    this.initData()
  },
  initConfig() {
    let parsedConfig = SliderBarParams.getSliderBarConfig(this.config);
    this.title = DefaultValueOperator.stringValue(parsedConfig.title, this.title);
    this.min = DefaultValueOperator.numberValue(parsedConfig.min, this.min);
    this.max = DefaultValueOperator.numberValue(parsedConfig.max, this.max);
    this.step = DefaultValueOperator.numberValue(parsedConfig.step, this.step);
    this.mode = DefaultValueOperator.numberValue(parsedConfig.mode, this.mode);
    this.showSubTitle = DefaultValueOperator.booleanValue(parsedConfig.showSubTitle, this.showSubTitle);
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = SliderBarParams.getSliderBarData(this.data)
    this.value = DefaultValueOperator.numberValue(parsedData.value, this.value)
    if (this.value < this.min) {
      this.value = this.min
    }
    if (this.value > this.max) {
      this.value = this.max
    }
    this.subTitle = this.value + this.unit;
  },
  onValueChange(changeEvent) {
    this.subTitle = changeEvent.value + this.unit;
    this.$emit('onChange', {
      value: changeEvent.value
    })
  }
}