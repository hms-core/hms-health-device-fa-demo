/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import TitleBarParams from './TitleBarParams'
import router from '@system.router'

const JS_TAG = 'JS/Component/Title bar: ';

export default {
  props: ['config', 'showObject'],
  computed: {
    showMenu() {
      return true;
    },
  },
  data: {
    title: '',
    subTitle: '',
    menuOptions: undefined,
    isDarkMode: false
  },
  onInit() {
    this.initSystemConfig();
    this.initComponentConfig();
  },
  initSystemConfig() {
    console.info(JS_TAG + 'globalData = ' + JSON.stringify(this.$app.$def.globalData));
    this.isDarkMode = this.$app.$def.globalData.isDarkMode;
  },
  initComponentConfig() {
    console.info(JS_TAG + 'onInit, config = ' + JSON.stringify(this.config));
    let parsedConfig = TitleBarParams.getTitleBarConfig(this.config)
    if (parsedConfig.title == undefined) {
      let productId = this.$app.$def.globalData.productId;
      if (productId != undefined) {
        this.title = this.$t('resources.' + productId).device_name;
      }
    } else {
      this.title = parsedConfig.title;
    }
    if (parsedConfig.subTitle == undefined || parsedConfig.subTitle == '') {
      this.subTitle = undefined;
    } else {
      this.subTitle = parsedConfig.subTitle;
    }
    this.menuOptions = parsedConfig.menuOptions;
  },
  back() {
    router.back();
  }
}