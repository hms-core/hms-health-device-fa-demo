/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ToggleButtonParams from './ToggleButtonParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Toggle Button : '

export default {
  props: ['config', 'data'],
  data: {
    defaultState: false,
    title: 'strings.toggle_button.default_title',
    activeIcon: '/common/img/base/ic_public_sound_active.png',
    inactiveIcon: '/common/img/base/ic_public_sound_inactive.png',
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
  },
  onReady() {
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let parsedConfig = ToggleButtonParams.getToggleButtonConfig(this.config)
    this.title = this.$t(DefaultValueOperator.stringValue(parsedConfig.title, this.title))
    this.subTitle = this.$t(DefaultValueOperator.stringValue(parsedConfig.subTitle, this.subTitle))
    this.activeIcon = DefaultValueOperator.imgPath(parsedConfig.activeIcon, this.activeIcon, this.$app.$def.globalData.isDarkMode)
    this.inactiveIcon = DefaultValueOperator.imgPath(parsedConfig.inactiveIcon, this.inactiveIcon, this.$app.$def.globalData.isDarkMode)
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    let parsedData = ToggleButtonParams.getToggleButtonData(this.data)
    this.defaultState = DefaultValueOperator.booleanValue(parsedData.defaultState, this.defaultState)
  },
  onChange() {
    this.defaultState = !this.defaultState;
    this.$emit('onChange', {
      value: this.state
    });
  }
}