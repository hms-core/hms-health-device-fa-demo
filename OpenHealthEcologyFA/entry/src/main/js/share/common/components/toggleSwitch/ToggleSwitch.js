/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ToggleSwitchParams from './ToggleSwitchParams.js'
import DefaultValueOperator from '../../scripts/defaultValueOperator.js'

const JS_TAG = 'JS/Component/Toggle Switch: '

export default {
  props: ['config', 'data'],
  data: {
    defaultState: false,
    title: 'strings.toggle_switch.default_title',
  },
  onInit() {
    this.$watch('config', 'configUpdate')
    this.$watch('data', 'dataUpdate')
    this.initConfig()
    this.initData()
  },
  configUpdate(newV, oldV) {
    this.initConfig()
  },
  dataUpdate(newV, oldV) {
    this.initData()
  },
  initConfig() {
    if (this.config == undefined) {
      return
    }
    let parsedConfig = ToggleSwitchParams.getToggleSwitchConfig(this.config);
    this.title = this.$t(DefaultValueOperator.stringValue(parsedConfig.title, this.title))
    this.subTitle = this.$t(DefaultValueOperator.stringValue(parsedConfig.subTitle, this.subTitle));
  },
  initData() {
    if (this.data == undefined) {
      return
    }
    this.defaultState = DefaultValueOperator.booleanValue(this.data.defaultState, this.defaultState);
  },
  onChange(changeEvent) {
    this.defaultState = changeEvent.checked;
    this.$emit('onChange', {
      value: this.defaultState
    })
  }
}