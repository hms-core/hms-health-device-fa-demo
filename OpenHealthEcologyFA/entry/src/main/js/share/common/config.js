/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const data = {
  // 帐号授权域列表
  requireScopeList: [
  ],
  // 必要帐号授权域列表，用户若未全部授予，则无法完成登录进行后续操作
  mandatoryScopeList: [
  ],
  // 应用中OAuth 2.0客户端ID（凭据）的Client ID，在创建应用后由华为开发者联盟为应用分配的唯一标识。
  clientId: '123456789',
  // 回调地址，用于应用服务器在获取用户授权后接收Authorization Code和获取凭证Access Token。
  redirectUri: 'http://127.0.0.1',
  // 应用中OAuth 2.0客户端ID（凭据）的Client Secret，在创建应用后由华为开发者联盟为应用分配公钥。
  clientSecret: '1234567812345678123456781234567812345678123456781234567812345678',
  // 动态申请的系统权限列表
  permissions: [
    'ohos.permission.LOCATION'
  ],
  // 必须获取的动态权限，若用户拒绝，则跳转到连接失败页面
  mandatoryPermissions: ['ohos.permission.LOCATION'],
  // toast弹窗显示时长，单位为毫秒
  toastDuration: 5000,
  // 连接设备前是否需要进行扫描的标识
  scanRequired: false,
  // 连接设备前是否需要进行智慧生活代理注册的标识
  proxyRegisterRequired: false,
  // 配对过程中需要注册监听的服务UUID
  serviceUuid: '00000000-0000-0000-0000-000000000000',
  // 配对过程中需要注册监听的特征值UUID
  characterUuid: '00000000-0000-0000-0000-000000000000',
  // 扫描时对设备广播的服务进行过滤，当且仅当设备广播的服务在过滤列表中才上报，该字段设置为空代表不过滤
  scanFilterUuid:['00000000-0000-0000-0000-000000000000'],
  // 默认的调试数据，用于本地拉起应用时替代NFC标签所传入的必要参数
  customDefaultData: {
    productId: 'A001',
    deviceMac: '00:00:00:00:00:00'
  }
}

export default data;