/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
import featureAbility from '@ohos.ability.featureAbility';
import wantConstant from '@ohos.ability.wantConstant';

const JS_TAG = 'JS/Script/abilityUtils: ';
const KEY_FROM_APP = '&fromApp=';
const PROXY_REGISTER_URI = 'hilink://hilinksvc.huawei.com/device?action=deviceAdd&prodId=';
const INTENT_ACTION_VIEW = 'android.intent.action.VIEW';

export default {
  openUrl(url) {
    let startAbilityParameter = {
      want: {
        action: INTENT_ACTION_VIEW,
        uri: url,
      },
    };
    console.info(JS_TAG + 'openUrl startAbilityParameter = ' + JSON.stringify(startAbilityParameter));
    return featureAbility.startAbility(startAbilityParameter);
  },
  performProxyRegister (uuid, productId, bundleName) {
    let startAbilityParameter = {
      want: {
        parameters: {
          uuid: uuid,
        },
        flag: wantConstant.Flags.FLAG_ABILITY_NEW_MISSION | wantConstant.Flags.FLAG_NOT_OHOS_COMPONENT,
        uri: PROXY_REGISTER_URI + productId + KEY_FROM_APP + bundleName
      },
    };
    console.info(JS_TAG + 'performProxyRegister startAbilityParameter = ' + JSON.stringify(startAbilityParameter));
    return featureAbility.startAbility(startAbilityParameter);
  },
}
