/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

interface BaseRequest {
    success?: (response?: BaseResponse) => void;
    fail?: (code: number, error: Error) => void;
}
interface BaseResponse {
    code: number;
    data?: any;
}
interface PermissionInfo {
    permissionState: PermissionStateInfo;
    requestCode: number;
}
interface PermissionStateInfo {
    availablePermissions: string[];
    grantedPermissions: string[];
    requestedPermissions: string[];
}
interface CheckPermissionData {
    permissions: string[];
}
interface PermissionData {
    permissions: string[];
}
interface AudioPlayData {
    audioFileList: string[];
}
interface CheckPermissionRequest extends BaseRequest, CheckPermissionData {
}
interface PermissionRequest extends BaseRequest, PermissionData {
}
interface AudioPlayRequest extends BaseRequest, AudioPlayData {
}
declare class CentralOperator {
    private static getRequestAction;
    private static processOperation;
    static checkPermission(request?: CheckPermissionRequest): Promise<BaseResponse>;
    static onPermissionRequest(callback: (object: PermissionInfo) => void): Promise<void>;
    static requestPermission(request?: PermissionRequest): Promise<BaseResponse>;
    static startAudioPlay(request?: AudioPlayRequest): Promise<void>;
    static stopAudioPlay(request?: BaseRequest): Promise<void>;
}
export default CentralOperator;
