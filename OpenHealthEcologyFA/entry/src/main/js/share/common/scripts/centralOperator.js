/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const TAG = 'JS/Operator/Central: ';
var AbilityType;
(function (AbilityType) {
    AbilityType[AbilityType["ABILITY_TYPE_EXTERNAL"] = 0] = "ABILITY_TYPE_EXTERNAL";
    AbilityType[AbilityType["ABILITY_TYPE_INTERNAL"] = 1] = "ABILITY_TYPE_INTERNAL";
})(AbilityType || (AbilityType = {}));
var SyncOption;
(function (SyncOption) {
    SyncOption[SyncOption["ACTION_SYNC"] = 0] = "ACTION_SYNC";
    SyncOption[SyncOption["ACTION_ASYNC"] = 1] = "ACTION_ASYNC";
})(SyncOption || (SyncOption = {}));
const BUNDLE_NAME = 'com.huawei.health.ecology.fa';
const ABILITY_NAME = 'com.huawei.health.ecology.fa.central.client.CentralJsClient';
var RequestCode;
(function (RequestCode) {
    RequestCode[RequestCode["CHECK_PERMISSION"] = 2001] = "CHECK_PERMISSION";
    RequestCode[RequestCode["ON_PERMISSION_REQUEST"] = 2002] = "ON_PERMISSION_REQUEST";
    RequestCode[RequestCode["REQUEST_PERMISSION"] = 2003] = "REQUEST_PERMISSION";
    RequestCode[RequestCode["START_AUDIO_PLAY"] = 2007] = "START_AUDIO_PLAY";
    RequestCode[RequestCode["STOP_AUDIO_PLAY"] = 2008] = "STOP_AUDIO_PLAY";
})(RequestCode || (RequestCode = {}));
var ResponseCode;
(function (ResponseCode) {
    ResponseCode[ResponseCode["OPERATION_SUCCESS"] = 10000] = "OPERATION_SUCCESS";
    ResponseCode[ResponseCode["OPERATION_FAILED"] = 10001] = "OPERATION_FAILED";
})(ResponseCode || (ResponseCode = {}));
class CentralOperator {
    static getRequestAction(requestCode) {
        let requestAction = {
            bundleName: BUNDLE_NAME,
            abilityName: ABILITY_NAME,
            abilityType: AbilityType.ABILITY_TYPE_INTERNAL,
            syncOption: SyncOption.ACTION_SYNC,
            messageCode: requestCode,
        };
        return requestAction;
    }
    static processOperation(request, action) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield FeatureAbility.callAbility(action);
                console.debug(TAG + 'Operation[' + action.messageCode + ']: ' + result);
                let response = JSON.parse(result);
                if (response.code == ResponseCode.OPERATION_SUCCESS) {
                    request.success(response);
                    return Promise.resolve(response);
                }
                else {
                    let error = {
                        name: "Operation[" + action.messageCode + ']: Falied',
                        message: ResponseCode[response.code] === undefined ? "Unknown Error." : ResponseCode[response.code],
                    };
                    request.fail(response.code, error);
                    return Promise.reject(error);
                }
            }
            catch (error) {
                request.fail(undefined, error);
                return Promise.reject(error);
            }
        });
    }
    static checkPermission(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.CHECK_PERMISSION);
            let actionData = {
                permissions: request.permissions,
            };
            action.data = actionData;
            return this.processOperation(request, action);
        });
    }
    static onPermissionRequest(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.ON_PERMISSION_REQUEST);
            let result = yield FeatureAbility.subscribeAbilityEvent(action, callbackData => {
                let response = JSON.parse(callbackData);
                callback(response.data);
            });
        });
    }
    static requestPermission(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.REQUEST_PERMISSION);
            let actionData = {
                permissions: request.permissions,
            };
            action.data = actionData;
            return this.processOperation(request, action);
        });
    }
    static startAudioPlay(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.START_AUDIO_PLAY);
            let actionData = {
                audioFileList: request.audioFileList,
            };
            action.data = actionData;
            this.processOperation(request, action);
        });
    }
    static stopAudioPlay(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let action = this.getRequestAction(RequestCode.STOP_AUDIO_PLAY);
            this.processOperation(request, action);
        });
    }
}
exports.default = CentralOperator;
