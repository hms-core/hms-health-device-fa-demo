/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var stringValue = (value, defaultValue) => {
  return (value == undefined || value == '') ? (defaultValue == undefined ? '' : defaultValue) : value;
}

var booleanValue = (value, defaultValue) => {
  return (value != undefined) ? value : (defaultValue == undefined ? false : defaultValue)
}

var numberValue = (value, defaultValue) => {
  return value ? value : (defaultValue == undefined ? 0 : defaultValue)
}

var arrayValue = (value, defaultValue) => {
  return value ? value : (defaultValue == undefined) ? [] : defaultValue
}

var imgPath = (value, defaultValue, isDarkMode) => {
  if (value == undefined || value == '') {
    if (isDarkMode) {
      return buildDarkImgPath(defaultValue)
    }
    return defaultValue
  } else {
    if (isDarkMode) {
      return buildDarkImgPath(value)
    }
    return value
  }
}

var buildDarkImgPath = (imgPath) => {
  let lastIndexPoint = imgPath.lastIndexOf('.')
  let imgPathPrefix = imgPath.substring(0, lastIndexPoint)
  let imgPathSuffix = imgPath.substring(lastIndexPoint, imgPath.length)
  return imgPathPrefix.concat('_dark').concat(imgPathSuffix)
}

export default {
  stringValue,
  booleanValue,
  numberValue,
  arrayValue,
  imgPath,
  buildDarkImgPath
};



