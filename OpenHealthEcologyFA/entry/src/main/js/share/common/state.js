/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
* 在此文件中，修改增加状态以适配特定流程。
* 修改后，需要进入main/src/main/js/connection/common/components中，
* 将对应组件中的showState修改增加需要显示的对应状态
*/
const data = {
  connection: {
    PRIVACY_CONTENT: 'PRIVACY_CONTENT', // 隐私协议内容界面
    PRIVACY: 'PRIVACY', // 隐私协议及登录页面
    PERMISSION_DENIED: 'PERMISSION_DENIED', // 权限禁用
    PERMISSION_FORBIDDEN: 'PERMISSION_FORBIDDEN', // 位置权限被禁止授予后，手动设置引导页面
    DISCOVERING: 'DISCOVERING', // 扫描中页面
    DISCOVER_FAILED: 'DISCOVER_FAILED', // 扫描失败页面
    CONNECTING: 'CONNECTING', // 连接中页面
    CONNECT_FAILED: 'CONNECT_FAILED', // 连接失败页面
    REDISCOVER_REQUESTED: 'REDISCOVER_REQUESTED', // 扫描失败，并已发起重新扫描请求
    RECONNECT_REQUESTED: 'RECONNECT_REQUESTED', // 连接失败，并已发起重连请求
  }
}

export default data;